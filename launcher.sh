#!/bin/bash
# use -X to have a GUI

if [ "-x" == "$1" ]; then
# echo "Graphique !"
    nbExp=1
    seed=1
    nbZIT=10
    nbZET=10
    nbEAM=3
    duration=300
    pfValue=800.0
    # We prepare the directories
    rm -r ./data/scenario*
    for i in `seq 1 $nbExp`; do
        mkdir `echo "./data/scenario$i"`
    done
    ./xpGenerator $nbExp $duration $seed $nbZIT $nbZET $nbEAM $pfValue AB_SCIENCE 10.385 ACCOR 34.02 EDF 11.57 LEGRAND 49.615

    for i in `seq 1 $nbExp`; do
        rm ./data/scenario.xml
        rm ./exp/ethicalHFTX.jcm
        cp `echo "./data/scenario$i/scenario.xml"` ./exp/data/scenario.xml
        cp `echo "./data/scenario$i/ethicalHFT.jcm"` ./exp/ethicalHFTX.jcm
        sed -i -- 's/marketSim\.controller\.NotGUIartifact/marketSim\.view\.MarketSimView/g' ./exp/ethicalHFTX.jcm
				cd exp
        #java -classpath ./jacamo-0.5/lib/jacamo.jar:./jacamo-0.5/lib/jason.jar jacamo.infra.RunJaCaMoProject ./exp/ethicalHFTX.jcm
				/usr/lib/jvm/java-13-jdk/bin/java -classpath ../jacamo-0.8/libs/ant-launcher-1.10.5.jar org.apache.tools.ant.launch.Launcher -e -f bin/ethicalHFTX.xml run
				cd ..
        mv ./exp/out/data `echo "./data/scenario$i/out"`
        mkdir ./exp/out/data
    done

else # TODO : debug this section
    nbExp=1
    seed=0
    nbZIT=100
    nbZET=50
    nbEAM=2
    duration=20
    pfValue=500.0
    # We prepare the directories
    rm -r ./data/scenario*
    for i in `seq 1 $nbExp`; do
        mkdir `echo "./data/scenario$i"`
    done

    # We generate files for simulations
    ./xpGenerator $nbExp $duration $seed $nbZIT $nbZET $nbEAM $pfValue AB_SCIENCE 10.385 ACCOR 34.02 AIR_FRANCE_-KLM 4.972 EDF 11.57 LEGRAND 49.615

    for i in `seq 1 $nbExp`; do
        rm ./data/scenario.xml
        rm ./exp/ethicalHFTX.jcm
        cp `echo "./data/scenario$i/scenario.xml"` ./exp/data/scenario.xml
        cp `echo "./data/scenario$i/ethicalHFT.jcm"` ./exp/ethicalHFT.jcm
        java -classpath ./jacamo-0.5/lib/jacamo.jar:./jacamo-0.5/lib/jason.jar jacamo.infra.RunJaCaMoProject ./exp/ethicalHFT.jcm > ./exp/out/data/log.txt
        mv ./exp/out/data `echo "./data/scenario$i/out"`
        mkdir ./exp/out/data
    done

    #java -classpath ./jacamo-0.5/lib/jacamo.jar:./jacamo-0.5/lib/jason.jar jacamo.infra.RunJaCaMoProject ./exp/ethicalHFT.jcm
fi
