

(obsolete) See instructions on [my personnal web page](http://nicolascointe.eu/projects/ethical_market_simulator) 

To run the project, first build the scenario generator (requires cmake and gcc)

```console
cd ExpGen
cmake -G "Unix Makefiles"
make
cd ..
cp ExpGen/bin/ExpGen xpGenerator
```

Then run the software with

```console
bash launcher.sh -x
```

