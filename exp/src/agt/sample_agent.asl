// Agent sample_agent in project ethicalHFT

{ include("$jacamoJar/templates/common-cartago.asl") }
{ include("$jacamoJar/templates/common-moise.asl") }

/* Initial beliefs */
state_of_market(xpar,close). //euronext is supposed to be close

/* Initial rules */


/* Initial goals */

!start.

/* Plans */

+!start : true
	<-	!situation_awareness;
		.print("hello world.").

+!situation_awareness : true
	<- .print("What happens ?").