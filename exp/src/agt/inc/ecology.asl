/* This asl file provide the theory of good concerning ecology as a virtue */

//* Values supports *//

// Value environment
~valueSupport(buy(Asset,_,_,_) ,"promote_renewable_energy") :- activity(Asset,"nuclear_energy_production").
~valueSupport(sell(Asset,_,_,_),"promote_renewable_energy") :- activity(Asset,"nuclear_energy_production").

valueSupport(buy(Asset,_,_,_) ,"environmental_reporting") :- label(Asset,"FSC").
valueSupport(sell(Asset,_,_,_),"environmental_reporting") :- label(Asset,"FSC").

valueSupport(buy(Asset,_,_,_) ,"fight_climate_change") :- activity(Asset,"nuclear_energy_production").
valueSupport(sell(Asset,_,_,_),"fight_climate_change") :- activity(Asset,"nuclear_energy_production").

// Categories of values
value("environment"). 
subvalue("promote_renewable_energy","environment").
subvalue("environmental_reporting","environment").
subvalue("fight_climate_change","environment").

//* Moral rules *//
moral_eval(X,V1,immoral):- ~valueSupport(X,V1) & subvalue(V1,"environment").
moral_eval(X,V1,moral):- valueSupport(X,V1) & subvalue(V1,"environment").
//moral_eval(X,"environment",moral):- valueSupport(X,"environment").


moralSet("environment","value_environment").
moralSet("promote_renewable_energy","value_environment").
moralSet("environmental_reporting","value_environment").
moralSet("fight_climate_change","value_environment").

//moralSet("avoid_energy_consumption","value_environment"). // DEBUG

moralSet(V1,MS):- moralSet(V2,MS) & subvalue(V1,V2).
thresholOnMoralSet(amoral,"value_environment").