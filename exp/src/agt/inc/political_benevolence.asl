/* This asl file provide the theory of good concerning political benevolence as a virtue */

//* Values supports *//

// Value environment
~valueSupport(buy(Asset,_,_,_) ,"fight anti-social finance") :- taxes_paid_in(Asset,Country) & tax_heaven(Country).
~valueSupport(sell(Asset,_,_,_),"fight anti-social finance") :- taxes_paid_in(Asset,Country) & tax_heaven(Country).

~valueSupport(buy(Asset,_,_,_) ,"political activity") :- memberOf(Asset,Group) & lobby(Group).
~valueSupport(sell(Asset,_,_,_),"political activity") :- memberOf(Asset,Group) & lobby(Group).

// Categories of values
value("political benevolence"). 
subvalue("fight anti-social finance","political benevolence").
subvalue("political activity","political benevolence").

//* Moral rules *//  TODO : à revoir
moral_eval(X,V1,immoral):- ~valueSupport(X,V1) & subvalue(V1,"political benevolence").
moral_eval(X,V1,moral):- valueSupport(X,V1) & subvalue(V1,"political benevolence").
moral_eval(X,"political benevolence",moral):- valueSupport(X,"political benevolence").