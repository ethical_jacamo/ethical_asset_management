
activity("EDF","nuclear_energy_production").
activity("LEGRAND","Diversified_Machinery").
activity("ACCOR","Lodging").
activity("AB SCIENCE","Drug Manufacturers - Major").

label("LEGRAND","FSC").

lobby("Bilderberg Group").
lobby("European Round Table of Industrialists").
lobby("European Services Forum").
lobby("International Chamber of Commerce").
lobby("Transatlantic Business Dialogue").
lobby("World Business Council for Sustainable Development").
lobby("World Economic Forum").