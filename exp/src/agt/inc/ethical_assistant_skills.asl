/* Initial goals */

!start.
!startAssist.

/* Plans */

+!start : true 
	<-	.my_name(N);
		.concat("art_",N,ArtN);
		makeArtifact(ArtN, "agentLink.AgentLinkArtifact", [N], ArtId);
		focus(ArtId);
		.wait(1000);
		.print("Hello World !");
		lookupArtifact("marketPlace",MarketPlaceId);
		linkArtifacts(ArtId,"out-1", MarketPlaceId);
		connect.
		
+!startAssist : assist(X) & role(R)
	<-	.my_name(N);
		.concat("hgui",X,ArtNhguiName);
		lookupArtifact(ArtNhguiName,ArtNGuiId);
		focus(ArtNGuiId);
		.print("I assist the human using ",ArtNhguiName," now.");
		connectGUI(N,R).
		
+!startAssist : true
	<-	.wait(1000);
		!startAssist.
		
+closed <- !harakiri.

+!harakiri : true
	<-	.my_name(N);
		.kill_agent(N).
		
+buy(Asset, Portfolio, Amount, Options)
	<-	+toBuy(Asset, Portfolio, Amount, Options).