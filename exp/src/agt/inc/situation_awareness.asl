// Agent situation_awareness in project ethicalHFT
// The following plans are activated by the AgentLinkArtifact to update beliefs of the agent

//---- Updating beliefs on owned assets ----//
+addInPortfolios (Portfolio,Broker,Asset,Amount)
	<- 	.my_name(N);
		//.concat(N, " : i have now ", Amount, " ", Asset,"\n",P);
		//print(P);
		.abolish(own(Portfolio,Broker,Asset,_));
		+own(Portfolio,Broker,Asset,Amount).
		
+erasePortfoliosBeliefs 
	<- .abolish(own(_,_,_,_)).

//---- Updating beliefs on market state ----//
+additionOrder (DateOfPlacement, Agent, Portfolio, Venue,Side,Asset,Amount,Price)
	<- +onMarket(DateOfPlacement, Agent, Portfolio, Venue,Side,Asset,Amount,Price).

+deletionOrder (DateOfPlacement, Agent, Portfolio, Venue,Side,Asset,Amount,Price) // When belief is deleted due to an update
	<- -onMarket(DateOfPlacement, Agent, Portfolio,Venue,Side,Asset,Amount,Price). 

+executionOrder (DateOfExecution, Agent, Portfolio, Venue, Side, Asset, Amount,Price)
	<- +executed(DateOfExecution, Agent, Portfolio, Venue, Side, Asset, Amount,Price);
		!forget.
	
+updateCanceledOrder(DateOfPlacement, Agent, Portfolio, Venue,Side,Asset,Amount,Price)
	<- -onMarket(DateOfPlacement, Agent, Portfolio, Venue,Side,Asset,Amount,Price).
	
+updateOrder (DateOfPlacement, Agent, Portfolio, Venue,Side,Asset,Amount,Price)
	<- +onMarket(DateOfPlacement, Agent, Portfolio, Venue,Side,Asset,Amount,Price).
	
// 		jia.differenceInMinutes("Wed Jun 01 17:02:14 CEST 2016","Wed Jun 01 17:00:12 CEST 2016",NMin);
+updateIndicator(DateOfEnd,Venue,Asset,Close,Volume,Intensity,Average,DoubleLengthAverage,BollingerUp,BollingerDown)
	<-  .abolish(indicators(_,Venue,Asset,_,_,_,_,_,_,_));
		+indicators(DateOfEnd,Venue,Asset,Close,Volume,Intensity,Average,DoubleLengthAverage,BollingerUp,BollingerDown).
		
	
// Until debug
+giveName(N)
	<- +myName(N).
	
+!forget : executed(DateOfExecution1,_,_,_,_,_,_,_) 
			& executed(DateOfExecution2,_,_,_,_,_,_,_) 
			& jia.differenceInMinutes(DateOfExecution1,DateOfExecution2,N)
			& N>=1
	<- .abolish(executed(DateOfExecution2,_,_,_,_,_,_,_));
		!forget.
		
+!forget : true <- true.