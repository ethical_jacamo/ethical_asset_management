// Agent rightness_process in project ethicalHFT

//*** Ethical principles ***//

// Perfect action : a moral, possible, and desired one
ethPrinciple(Agent,"perfectAct",Action):-
	possible_eval(Agent,Action, possible) &
	desire_eval(Agent,Action,desired) &
	not desire_eval(Agent,Action,undesired) &
	moral_eval(Agent,Action,_,moral) &
	not moral_eval(Agent,Action,_,immoral).

// Desire No Regret : a desirable but not immoral action
ethPrinciple(Agent,"desireNR",Action):-
	possible_eval(Agent,Action, possible) &
	desire_eval(Agent,Action,desired) &
	not desire_eval(Agent,Action,undesired) &
	not moral_eval(Agent,Action,_,immoral).
	
// Duty No Regret : a Moral but not undesirable action
ethPrinciple(Agent,"dutyNR",Action):-
	possible_eval(Agent,Action, possible) &
	not desire_eval(Agent,Action,undesired) &
	moral_eval(Agent,Action,_,moral) &
	not moral_eval(Agent,Action,_,immoral).

// Aristotelian ethics inspired from
//	- [1] J.G. Ganascia, "Modelling ethical rules of lying with Answer Set Programming"
//  - [2] J.G. Ganascia, "Ethical System Formalization using Non-Monotonic Logics"
ethPrinciple(Agent,"aristotelian",Action):-
	possible_eval(Agent,Action, possible) &
	aristotelian_just(Agent,Action).

// These Rules are directly translated from [1]
aristotelian_just(Agent,Action):-
	not aristotelian_unjust(Agent,Action).
	
aristotelian_unjust(Agent,Action):-
	aristotelian_worst_consequence(Action, Consequence)
	& aristotelian_worst_consequence(OtherAction, ConsequenceOtherAction)
	& aristotelian_less_moral(Agent,Consequence, ConsequenceOtherAction).
	
aristotelian_worst_consequence(Action,Consequence):-
	aristotelian_consequence(Action,Consequence)
	& not aristotelian_not_worst_consequence(Action, Consequence).
	
aristotelian_not_worst_consequence(Action, Consequence):-
	aristotelian_consequence(Action, Consequence)
	& aristotelian_consequence(Action, OtherConsequence)
	& aristotelian_less_moral(OtherConsequence, Consequence)
	& not aristotelian_less_moral(Consequence, OtherConsequence).
	
aristotelian_consequence(Action,Action).
	
// Here we adapt these rules in our model
aristotelian_consequence(Action,Consequence):- consequence(Action, Consequence).

aristotelian_less_moral(Agent,E1, E2):-
	moral_eval(Agent,E1,V1,MV1)
	& moral_eval(Agent,E2,V2,MV2)
	& torder_on_moral_valuation(MV2, MV1)
	& not aristotelian_not_less_moral(Agent,E1, E2).
	
aristotelian_not_less_moral(Agent,E1, E2):-
	moral_eval(Agent,E1,V1,MV1)
	& moral_eval(Agent,E2,V2,MV2)
	& not torder_on_moral_valuation(MV2, MV1)
	& not equals_actions(E1,E2).
	
equals_actions(buy(A1,B1,C1,D1),buy(A2,B2,C2,D2)):- // TODO : complete this comparison
	asset(A,A1)
	& asset(A,A2).

equals_actions(sell(A1,B1,C1,D1),sell(A2,B2,C2,D2)):- // TODO : complete this comparison
	asset(A,A1)
	& asset(A,A2).
	
//*** Ethical Preferences ***//
// This section describe transitivity on the preferences

tPrefEthics(PE1,PE2):- prefEthics(PE1,PE2).
tPrefEthics(PE1,PE2):- prefEthics(PE1,PE3) & tPrefEthics(PE3,PE2).

//*** Ethical Judgment ***//

existBetter(A,PE1,X):- ethPrinciple(A,PE1,X) & tPrefEthics(PE2,PE1) & ethPrinciple(A,PE2,Y) & not ethPrinciple(A,PE1,Y).

ethicalJudgment(A,X,PE):- ethPrinciple(A,PE,X) & not existBetter(A,PE,X).
