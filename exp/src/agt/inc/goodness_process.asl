// Goodness Process of agents in project ethicalHFT

// Order on moral valuations

order_on_moral_valuation(very_moral, moral).
order_on_moral_valuation(moral, amoral).
order_on_moral_valuation(amoral, immoral).
order_on_moral_valuation(immoral, very_immoral).

// Transitivity
torder_on_moral_valuation(MV1, MV2):- order_on_moral_valuation(MV1, MV2).
torder_on_moral_valuation(MV1, MV2):- order_on_moral_valuation(MV1, MV3) & torder_on_moral_valuation(MV3, MV2). 