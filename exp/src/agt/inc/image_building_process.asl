+executed(DateOfExecution, Agent, Portfolio, Venue, "bid", Asset, Amount,Price)
	<-	+evaluateEthics(DateOfExecution, Agent,buy(Asset,Amount,Portfolio,"MKT"));
	 	+evaluateMorality(DateOfExecution, Agent,buy(Asset,Amount,Portfolio,"MKT"));
	 	//.print("Let's go to evaluate !",DateOfExecution,Agent,Asset,Amount,Price);
	 	!evaluateEthics;
	 	!evaluateMorality
	 	.
	 	
+executed(DateOfExecution, Agent, Portfolio, Venue, "ask", Asset, Amount,Price)
	<-	+evaluateEthics(DateOfExecution, Agent,sell(Asset,Amount,Portfolio,"MKT"));
	 	+evaluateMorality(DateOfExecution, Agent,sell(Asset,Amount,Portfolio,"MKT"));
	 	//.print("Let's go to evaluate !");
	 	!evaluateEthics;
	 	!evaluateMorality.
	 	
/** --- AGGREGATION ON ETHICS --- **/	 
	 
// judged conform
	// their is an image of the agent before
	+!evaluateEthics :
		evaluateEthics(DateOfExecution, Agent,Action) 
		& ethicalJudgment(Agent,Action,PE)
		& ethicalAggr(Agent,X)
		<-	-evaluateEthics(DateOfExecution, Agent, Action);
			.abolish(ethicalAggr(Agent,_));
			+ethicalAggr(Agent,X+1);
			+rebuildImageOf(Agent);
			!evaluateEthics.
		
	// their is no image of the agent yet
	+!evaluateEthics : 
		evaluateEthics(DateOfExecution, Agent, Action) 
		& ethicalJudgment(Agent,Action,PE)
		<-	-evaluateEthics(DateOfExecution, Agent, Action);
			+ethicalAggr(Agent,1);
			+unethicalAggr(Agent,0);
			+rebuildImageOf(Agent);
			!evaluateEthics.
		
// not judged conform
	// their is an image of the agent before
	+!evaluateEthics :
		evaluateEthics(DateOfExecution, Agent, Action)
		& unethicalAggr(Agent,X)
		<-	-evaluateEthics(DateOfExecution, Agent, Action);
			.abolish(unethicalAggr(Agent,_));
			+unethicalAggr(Agent,X+1);
			+rebuildImageOf(Agent);
			!evaluateEthics.
		
	// their is no image of the agent yet		
	+!evaluateEthics :
		evaluateEthics(DateOfExecution, Agent, Action)
		<-	-evaluateEthics(DateOfExecution, Agent, Action);
			+unethicalAggr(Agent,1);
			+ethicalAggr(Agent,0);
			+rebuildImageOf(Agent);
			!evaluateEthics.
		
+!evaluateEthics : true
			<- !buildEthicalImage.

+!buildEthicalImage : rebuildImageOf(Agent)
			& unethicalAggr(Agent,U)
			& ethicalAggr(Agent,E)
			& E/(U+E)<0.4
			<- .abolish(ethicalImage(Agent,_));
				changeImageof(Agent,"ethics",E,U); // Write the current image in the log file
				-rebuildImageOf(Agent);
				+ethicalImage(Agent,improper);
				!buildEthicalImage.
				
+!buildEthicalImage : rebuildImageOf(Agent)
			& unethicalAggr(Agent,U)
			& ethicalAggr(Agent,E)
			& E/(U+E)<0.6
			<- .abolish(ethicalImage(Agent,_));
				changeImageof(Agent,"ethics",E,U); // Write the current image in the log file
				-rebuildImageOf(Agent);
				+ethicalImage(Agent,neutral);
				!buildEthicalImage.
				
+!buildEthicalImage : rebuildImageOf(Agent)
			& unethicalAggr(Agent,U)
			& ethicalAggr(Agent,E)
			<-	.abolish(ethicalImage(Agent,_));
				changeImageof(Agent,"ethics",E,U); // Write the current image in the log file
				-rebuildImageOf(Agent);
				+ethicalImage(Agent,congruent);
				!buildEthicalImage.

+!buildEthicalImage : true.


/** --- AGGREGATION OF MORALITY --- **/
// These plans generates an "evaluateMoralConformity" belief for each combinations action - moralSet
// They also initialize the aggregated values if this is the first evaluation of the agent 

// an order is executed
	// their is an image of the agent before
	+!evaluateMorality :
		evaluateMorality(_, Agent, Action)
		& thresholOnMoralSet(MoralThreshold,MoralSet)
		& moralAggr(Agent,MoralSet,_) // An image already exists
		& not evaluateMoralConformity(Agent,Action,MoralSet,MoralThreshold)
		<-  +evaluateMoralConformity(Agent,Action,MoralSet,MoralThreshold);
			!evaluateMorality.
			
 	// their is no image of the agent yet
	+!evaluateMorality :
		evaluateMorality(_, Agent, Action)
		& thresholOnMoralSet(MoralThreshold,MoralSet)
		& not evaluateMoralConformity(Agent,Action,MoralSet,MoralThreshold)
		<-	+evaluateMoralConformity(Agent,Action,MoralSet,MoralThreshold);
			+moralAggr(Agent,MoralSet,0);
			+immoralAggr(Agent,MoralSet,0);
			!evaluateMorality.
		
+!evaluateMorality : evaluateMorality(DateOfExecution, Agent, Action)
	<- -evaluateMorality(DateOfExecution, Agent, Action);
		!evaluateMorality.
				
+!evaluateMorality : true
	<- !evaluateMoralConformity.
	
// evaluation of the moral conformity
	// Evaluates the conformity of the executed action regarding each moral rule

	// if the action is conform
+!evaluateMoralConformity :
	evaluateMoralConformity(Agent,Action,MoralSet,MoralThreshold)
	& not evaluated(Agent,Action,MoralRule,MoralThreshold)
	& isMorallyConform(Agent,Action,MoralRule,MoralThreshold)
	& moralSet(MoralRule,MoralSet)
	& moralAggr(Agent,MoralSet,X)
	<- 	+evaluated(Agent,Action,MoralRule,MoralThreshold);
		.abolish(moralAggr(Agent,MoralSet,_));
		+moralAggr(Agent,MoralSet,X+1);
		+rebuildMoralImageOf(Agent,MoralSet);
		.print("It is conform with ",MoralRule," to ",Action);
		!evaluateMoralConformity.
		
	// if the action is not conform
+!evaluateMoralConformity :
	evaluateMoralConformity(Agent,Action,MoralSet,MoralThreshold)
	& not evaluated(Agent,Action,MoralRule,MoralThreshold)
	& isMorallyNotConform(Agent,Action,MoralRule,MoralThreshold)
	& moralSet(MoralRule,MoralSet)
	& immoralAggr(Agent,MoralSet,X)
	<- 	+evaluated(Agent,Action,MoralRule,MoralThreshold);
		.abolish(immoralAggr(Agent,MoralSet,_));
		+immoralAggr(Agent,MoralSet,X+1);
		+rebuildMoralImageOf(Agent,MoralSet);
		.print("It is not conform with ",MoralRule," to ",Action);
		!evaluateMoralConformity.
	
+!evaluateMoralConformity : 
	evaluateMoralConformity(Agent,Action,MoralRule,MoralThreshold)
	<- 	-evaluateMoralConformity(Agent,Action,MoralSet,MoralThreshold);
		.abolish(evaluated(Agent,Action,_,MoralThreshold));
		!evaluateMoralConformity.
		
+!evaluateMoralConformity : true
	<- !buildMoralImage.

// Building Moral image

+!buildMoralImage : rebuildMoralImageOf(Agent,MoralSet)
			& immoralAggr(Agent,MoralSet,U)
			& moralAggr(Agent,MoralSet,M)
			& M/(U+M)<0.4
			<- .abolish(moralImageOf(Agent,MoralSet,_));
				changeImageof(Agent,MoralSet,M,U); // Write the current image in the log file
				-rebuildMoralImageOf(Agent,MoralSet);
				+moralImageOf(Agent,MoralSet,improper);
				!buildMoralImage.
				
+!buildMoralImage : rebuildMoralImageOf(Agent,MoralSet)
			& immoralAggr(Agent,MoralSet,U)
			& moralAggr(Agent,MoralSet,M)
			& M/(U+M)<0.6
			<- .abolish(moralImageOf(Agent,MoralSet,_));
				changeImageof(Agent,MoralSet,M,U); // Write the current image in the log file
				-rebuildMoralImageOf(Agent,MoralSet);
				+moralImageOf(Agent,MoralSet,neutral);
				!buildMoralImage.
				
+!buildMoralImage : rebuildMoralImageOf(Agent,MoralSet)
			& immoralAggr(Agent,MoralSet,U)
			& moralAggr(Agent,MoralSet,M)
			<-	.abolish(moralImageOf(Agent,MoralSet,_));
				changeImageof(Agent,MoralSet,M,U); // Write the current image in the log file
				-rebuildMoralImageOf(Agent,MoralSet);
				+moralImageOf(Agent,MoralSet,congruent);
				!buildMoralImage.
				
+!buildMoralImage : rebuildMoralImageOf(Agent,MoralSet) // 0 ethical action executed yet
			& immoralAggr(Agent,MoralSet,U)
			& moralAggr(Agent,MoralSet,M)
			<-	.abolish(moralImageOf(Agent,MoralSet,_));
				changeImageof(Agent,MoralSet,M,U); // Write the current image in the log file
				-rebuildMoralImageOf(Agent,MoralSet);
				+moralImageOf(Agent,MoralSet,improper);
				!buildMoralImage.

+!buildMoralImage : true.

// Check if the moral rule's context is true and if the action is conform or not
isMorallyConform(Agent,Action,MoralRule,MoralThreshold):-
	moral_eval(Agent,Action,MoralRule,MoralValuation)
	& not isMorallyNotConform(Agent,Action,MoralRule,MoralThreshold).
	
isMorallyNotConform(Agent,Action,MoralRule,MoralThreshold):-
	moral_eval(Agent,Action,MoralRule,MoralValuation)
	& torder_on_moral_valuation(MoralThreshold,MoralValuation).
	
//*** Conformity Valuations ***//
// This section describe transitivity on the order of conformity valuations

tOrderOnConformityValuation(CV1,CV2):- orderOnConformityValuation(CV1,CV2).
tOrderOnConformityValuation(CV1,CV2):- orderOnConformityValuation(CV1,CV3) & tOrderOnConformityValuation(CV3,CV2).

	