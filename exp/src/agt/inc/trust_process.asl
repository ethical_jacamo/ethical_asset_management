


+!trust : running(true)
	//& ethicalJudgment(trust(Agent,MoralSet),PE) // TODO
	& timeloop(T)
	& moralImageOf(Agent,MoralSet,ConformityValuation)
	& trustThreshold(Threshold)
	& not tOrderOnConformityValuation(Threshold,ConformityValuation)
	& not trust(Agent,MoralSet)
	<-	.print("I decide now to trust ", Agent , " according with ", PE);
		+trust(Agent,MoralSet);
		.wait(T);
		!trust.	
		
+!trust : true
	<- .wait(1000);
		!trust.