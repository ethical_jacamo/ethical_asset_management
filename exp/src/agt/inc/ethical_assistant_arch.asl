// Add ontology
{ include("inc/ontology.asl") }
// Add situation awareness
{ include("inc/situation_awareness.asl") }
// Add evaluation_process
{ include("inc/evaluation_process.asl") }
// Add moral valuations
{ include("inc/goodness_process.asl") }
// Add ethical principles
{ include("inc/rightness_process.asl") }

// Add special skills for ethical assistants
{ include("inc/ethical_assistant_skills.asl") }