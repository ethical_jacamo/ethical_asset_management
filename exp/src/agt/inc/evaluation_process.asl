// Agent evaluation_process in project ethicalHFT

			//*** desirability evaluation ***//


// hypothesis on the desires of the others
//desire_eval(Agent,Action,Eval):-desire_eval(N,Action,Eval) & .my_name(N).

// We desire to buy an asset if average > double average & close < bollinger up
// Because the price is supposed to grow up
desire_eval(N,buy(Asset, 10, Portfolio, "MKT"),desired):-
	myName(N)
	& not onMarket(_,N, _, Venue, "bid",Asset,_,_) // I am not yet a buyer
	& Asset \== "EURO"
	& own(Portfolio,_,"EURO",Amount) // I have enough money
	& indicators(_,Venue,Asset,Close,_,_,Average,DoubleLengthAverage,BollingerUp,BollingerDown)
	& Average>DoubleLengthAverage // it's growing up
	& Close<BollingerUp
	& Close>BollingerDown.
	
desire_eval(N,buy(Asset, 10, Portfolio, "MKT"),undesired):-
	myName(N)
	& not onMarket(_,N, _, Venue, "bid",Asset,_,_) // I am not yet a buyer
	& Asset \== "EURO"
	& own(Portfolio,_,"EURO",Amount) // I have enough money
	& indicators(_,Venue,Asset,Close,_,_,Average,DoubleLengthAverage,BollingerUp,BollingerDown)
	& Average<=DoubleLengthAverage // it's growing up
	& Close<BollingerUp
	& Close>BollingerDown.
	
// We desire to buy an asset if close < bollinger down
// Because it announce probably a breakout
desire_eval(N,buy(Asset, 10, Portfolio, "MKT"),desired):-
	myName(N)
	& not onMarket(_,N, _, Venue, "bid",Asset,_,_) // I am not yet a buyer
	& Asset \== "EURO"
	& own(Portfolio,_,"EURO",Amount) // I have enough money
	& indicators(_,Venue,Asset,Close,_,_,_,_,_,BollingerDown)
	& sensibility(X)
	& Close-BollingerDown<X/100. // Possible breakout 
	
// We desire to sell an asset if average < double average & close > bollinger down
// Because the price is supposed to fall
desire_eval(N,sell(Asset, Amount, Portfolio, "MKT"),desired):-
	myName(N)
	& not onMarket(_,N, _, Venue, "ask",Asset,_,_) // I am not yet a seller
	& Asset \== "EURO"
	& own(Portfolio,_,Asset,Amount)	// I have some asset to sell 
	& indicators(_,Venue,Asset,Close,_,_,Average,DoubleLengthAverage,BollingerUp,BollingerDown)
	& Average<=DoubleLengthAverage // it's falling or staying at the same position
	& Close>BollingerDown
	& Close<BollingerUp.
	
desire_eval(N,sell(Asset, Amount, Portfolio, "MKT"),undesired):-
	myName(N)
	& not onMarket(_,N, _, Venue, "ask",Asset,_,_) // I am not yet a seller
	& Asset \== "EURO"
	& own(Portfolio,_,Asset,Amount)	// I have some asset to sell 
	& indicators(_,Venue,Asset,Close,_,_,Average,DoubleLengthAverage,BollingerUp,BollingerDown)
	& Average>DoubleLengthAverage // it's falling or staying at the same position
	& Close>BollingerDown
	& Close<BollingerUp.
	
// We desire to sell an asset if close > bollinger up
// Because it announce probably a breakdown
desire_eval(N,sell(Asset, Amount, Portfolio, "MKT"),desired):-
	myName(N)
	& not onMarket(_,N, _, Venue, "ask",Asset,_,_) // I am not yet a seller
	& Asset \== "EURO"
	& own(Portfolio,_,Asset,Amount)	// I have some asset to sell 
	& indicators(_,Venue,Asset,Close,_,_,Average,_,BollingerUp,_)
	& sensibility(X)	
	& Close-BollingerUp>X/100.
	
// We desire to sell an asset if BollingerUp == BollingerDown && Average>BblAverage
// Because it announce probably a breakdown
desire_eval(N,sell(Asset, Amount, Portfolio, "MKT"),desired):-
	myName(N)
	& not onMarket(_,N, _, Venue, "ask",Asset,_,_) // I am not yet a seller
	& Asset \== "EURO"
	& own(Portfolio,_,Asset,Amount)	// I have some asset to sell 
	& indicators(_,Venue,Asset,Close,_,_,Average,DoubleLengthAverage,BollingerUp,BollingerDown)
	& BollingerUp-BollingerDown<0.01
	& Average>=DoubleLengthAverage.
	
			//*** capability evaluation ***//

possible_eval(N,buy(Asset, Q, Portfolio, "MKT"), possible):-
	myName(N)
	& running(true)
	& Asset \== "EURO"
	& own(Portfolio,_,"EURO",Amount) // I have enough money
	& indicators(_,_,Asset,_,_,_,Average,_,_,_)
	& Q<=Amount/(Average*1000)+1
	& Q>=0.
	

possible_eval(N,sell(Asset, Q, Portfolio, "MKT"), possible):-
	myName(N)
	& running(true)
	& Asset \== "EURO"
	& own(Portfolio,_,Asset,Amount) // I have enough assets
	& Q<=Amount
	& Q>=0.
	
// possible_eval(N,trustBuild(Agent,MoralSet), possible):- moralImageOf(Agent,MoralSet,_) & myName(N).
	