// Agent simulatorAgent in project ethicalHFT

{ include("$jacamoJar/templates/common-cartago.asl") }
{ include("$jacamoJar/templates/common-moise.asl") }

/* Initial beliefs and rules */
nhgui(0).

/* Initial goals */

!start.

/* Plans */

+!start : true 
	<-	.my_name(N);
		.concat("art_",N,ArtN);	
		makeArtifact(ArtN, "agentLink.AgentLinkArtifact", [N], ArtId);
		focus(ArtId);
		.wait(1000);
		lookupArtifact("marketPlace",MarketPlaceId);
		linkArtifacts(ArtId,"out-1", MarketPlaceId);
		focus(MarketPlaceId);
		connect;
		lookupArtifact("clock",ClockId);
		focus(ClockId);
		lookupArtifact("gui",GuiId);
		focus(GuiId).

// Updating beliefs on owned assets
+addInPortfolios (Portfolio,Broker,Asset,Amount)
	<- 	.my_name(N);
		.abolish(own(Portfolio,Broker,Asset,_));
		+own(Portfolio,Broker,Asset,Amount).
		
+erasePortfoliosBeliefs <- .abolish(own(_,_,_,_)).

+createTradingInterface <- !createTradingInterface.

+!createTradingInterface <-
		?nhgui(N);
		Nhgui=N+1;
		-+nhgui(Nhgui);
		/* Creation of the assistants */
			// The first one do not have any rules nor values
		.concat("ethical_assistant_",Nhgui,AgentName);
		.create_agent(AgentName,"ethicalAssistant.asl");
		
			// The second one is ecologist
		.concat("ethical_assistant_ecologist_",Nhgui,AgentNameEco);
		.create_agent(AgentNameEco,"ethicalAssistantEcologist.asl");
		
			// The third one is politically benevolent
		.concat("ethical_assistant_political_benevolence_",Nhgui,AgentNamePol);
		.create_agent(AgentNamePol,"ethicalAssistantPoliticalBenevolent.asl");
		
		/* Creation of the GUI artifact for the human */
		.concat("hgui",Nhgui,ArtNhguiName);
		makeArtifact(ArtNhguiName, "humanTraderGUI.HumanTraderGUI", [], ArtId);
		subscribe(ArtId); // We add the artifact in the list of subscribers of the clock
		subscribeHumanTraderGUI(ArtId,ArtNhguiName); // We add the artifact in the list of humanTradingGUI of the market
		.concat(ArtNhguiName,"'s default Portfolio",PfName);
		createPortfolio(PfName,ArtNhguiName,"b1",["EURO",10000000]);
		
		/* Assign to assistants their mission */
		.send(AgentName,tell,assist(Nhgui));
		.send(AgentNameEco,tell,assist(Nhgui));
		.send(AgentNamePol,tell,assist(Nhgui));
		.
		
		