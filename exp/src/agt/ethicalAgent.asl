// Agent ethicalAgent in project ethicalHFT

{ include("$jacamoJar/templates/common-cartago.asl") }
{ include("$jacamoJar/templates/common-moise.asl") }

{ include("inc/ethical_agent_arch.asl") }

//* Initial beliefs and rules *//
{ include("inc/common_belief_base.asl") }

//* Values supports *//

// Value environment
~valueSupport(buy(Asset,_,_,_) ,"promote_renewable_energy") :- activity(Asset,"nuclear_energy_production").
~valueSupport(sell(Asset,_,_,_),"promote_renewable_energy") :- activity(Asset,"nuclear_energy_production").

//~valueSupport(buy(Asset,_,_,_) ,"avoid_energy_consumption") :- activity(Asset,_). // DEBUG
//~valueSupport(sell(Asset,_,_,_),"avoid_energy_consumption") :- activity(Asset,_). // DEBUG

valueSupport(buy(Asset,_,_,_) ,"environmental_reporting") :- label(Asset,"FSC").
valueSupport(sell(Asset,_,_,_),"environmental_reporting") :- label(Asset,"FSC").

valueSupport(buy(Asset,_,_,_) ,"fight_climate_change") :- activity(Asset,"nuclear_energy_production").
valueSupport(sell(Asset,_,_,_),"fight_climate_change") :- activity(Asset,"nuclear_energy_production").

// Categories of values
value("environment"). 
subvalue("promote_renewable_energy","environment").
subvalue("environmental_reporting","environment").
subvalue("fight_climate_change","environment").

//subvalue("avoid_energy_consumption","environment"). // DEBUG

//* Moral rules *//
moral_eval(_,X,V1,immoral):- ~valueSupport(X,V1) & subvalue(V1,"environment").
moral_eval(_,X,V1,moral):- valueSupport(X,V1) & subvalue(V1,"environment").
//moral_eval(_,X,"environment",moral):- valueSupport(X,"environment").

moral_eval(_,trustBuild(_,_),"responsibility",amoral).
//desire_eval(trust(_,_),"trust",desired).

moralSet("environment","value_environment").
moralSet("promote_renewable_energy","value_environment").
moralSet("environmental_reporting","value_environment").
moralSet("fight_climate_change","value_environment").

//moralSet("avoid_energy_consumption","value_environment"). // DEBUG

moralSet(V1,MS):- moralSet(V2,MS) & subvalue(V1,V2).
thresholOnMoralSet(amoral,"value_environment").

//* Ethical preferences *//
prefEthics("aristotelian","perfectAct").
prefEthics("perfectAct","desireNR").
prefEthics("desireNR","dutyNR").

//* Conformity valuations *//
orderOnConformityValuation(congruent,neutral).
orderOnConformityValuation(neutral,improper).

trustThreshold(congruent).

countdown(5).
timeloop(2500).

/* Initial goals */

!start.
!trade.
!trust.
!subscribe.
!truth.

/* DEBUG */

+!truth : isMorallyNotConform("TEST",sell("ACCOR",1,"portfolioEAM1","MKT"),MoralRule,amoral)
	<- .print("It is not moral not conform to ",sell("LEGRAND",1,"portfolioEAM1","MKT"), " due to ", MoralRule).
	
+!truth : moral_eval("TEST",sell("LEGRAND",1,"portfolioEAM1","MKT"),V1,immoral)
	<- .print("It is not moral to ",sell("LEGRAND",1,"portfolioEAM1","MKT"), " due to ", V1).
	
+!truth : true.

/* Plans */

+!start : true 
	<-	.my_name(N);
		.concat("art_",N,ArtN);
		makeArtifact(ArtN, "agentLink.AgentLinkArtifact", [N], ArtId);
		focus(ArtId);
		giveMeMyName;
		.wait(1000);
		.print("Hello World !");
		lookupArtifact("marketPlace",MarketPlaceId);
		linkArtifacts(ArtId,"out-1", MarketPlaceId);
		+patience(4);
		.random(X);
		+sensibility(X);
		connect.

// subscribe as soon as possible		
+!subscribe : not running(true)
	<- .wait(1000);
		!subscribe.
		
+!subscribe : running(true)
	<- 	subscribeForAssetInformation("EURONEXT - EURONEXT PARIS","AB SCIENCE");
		subscribeForAssetInformation("EURONEXT - EURONEXT PARIS","ACCOR");
		subscribeForAssetInformation("EURONEXT - EURONEXT PARIS","EDF");
		subscribeForAssetInformation("EURONEXT - EURONEXT PARIS","LEGRAND").
		
// Trade randomly		
+!trade : not running(true)
	<-	.wait(1000);
		!trade.
	
// Sell if ethical and yet a seller
+!trade : running(true)
	& own(Portfolio,_,Asset,Amount)	// I have some asset to sell
	& myName(N)
	& onMarket(Date,N, Portfolio, Venue, "ask",Asset, Qty, Price) // I'm yet a seller
	& ethicalJudgment(N,sell(Asset, Amount, Portfolio, "MKT"),PE)
	& patience(Z)
	& timeloop(T)
	<-	cancelOrder(Date,Portfolio,Venue,"ask",Asset, Qty, Price);
		.print("I want to sell ", Amount, " ", Asset, " according with ", PE);
		sell(Asset, Amount, Portfolio, "MKT"); 
		.wait(T);
		-+countdown(Z);
		!trade.	
		
// Sell if ethical and not yet a seller
+!trade : running(true)
	& own(Portfolio,_,Asset,Amount)	// I have some asset to sell
	& myName(N) 
	& ethicalJudgment(N,sell(Asset, Amount, Portfolio, "MKT"),PE)
	& patience(Z)
	& timeloop(T)
	<-	.print("I want to sell ", Amount, " ", Asset, " according with ", PE);
		sell(Asset, Amount, Portfolio, "MKT"); 
		.wait(T);
		-+countdown(Z);
		!trade.	
	
// Buy if ethical and yet a buyer
+!trade : running(true)
	& onMarket(_,_, _, _, "ask",Asset,Amount,_) // There is a seller on the market
	& myName(N)
	& onMarket(Date,N, Portfolio, Venue, "bid",Asset, Qty, Price) // I'm yet a buyer
	& ethicalJudgment(N,buy(Asset, Amount, Portfolio, "MKT"),PE)
	& patience(Z)
	& timeloop(T)
	<-	cancelOrder(Date,Portfolio,Venue,"bid",Asset, Qty, Price);
		.print("I want to buy ", Amount, " ", Asset, " according with ", PE);
		buy(Asset, Amount, Portfolio, "MKT"); 
		.wait(T);
		-+countdown(Z);
		!trade.
		
// Buy if ethical
+!trade : running(true)
	& onMarket(_,_, _, _, "ask",Asset,Amount,_) // There is a seller on the market
	& myName(N)
	& ethicalJudgment(N,buy(Asset, Amount, Portfolio, "MKT"),PE)
	& patience(Z)
	& timeloop(T)
	<-	.print("I want to buy ", Amount, " ", Asset, " according with ", PE);
		buy(Asset, Amount, Portfolio, "MKT"); 
		.wait(T);
		-+countdown(Z);
		!trade.		
		
+!trade : running(true) & countdown(Z) & Z>0 & timeloop(T)
	<- .wait(T);
		-+countdown(Z-1);
		!trade.
		
+!trade : running(true) & myName(N)	& timeloop(T) & countdown(Z) & Z<=0 & onMarket(Date,N,Pf, Venue, Side, Asset, Qty, Price)
	<- 	cancelOrder(Date,Pf,Venue,Side,Asset,Qty,Price);
		.wait(T);
		!trade.

+!trade : running(true) & timeloop(T)
	<- .wait(T);
		!trade.

