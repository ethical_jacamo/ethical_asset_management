// Agent zero_intelligence_asset_manager in project ethicalHFT

{ include("$jacamoJar/templates/common-cartago.asl") }
{ include("$jacamoJar/templates/common-moise.asl") }
{ include("inc/situation_awareness.asl")}

/* Initial beliefs and rules */

trade(X):- buy(X).
trade(X):- sell(X).

existsBetter(Venue,"bid",Asset,Price):- onMarket(_, _, _, Venue,"bid",Asset,_,Price2) & Price < Price2.
existsBetter(Venue,"ask",Asset,Price):- onMarket(_, _, _, Venue,"ask",Asset,_,Price2) & Price > Price2.


/* Initial goals */

!start.
!subscribe.

/* Plans */

+!start : true 
	<-	.my_name(N);
		.concat("art_",N,ArtN);	
		makeArtifact(ArtN, "agentLink.AgentLinkArtifact", [N], ArtId);
		focus(ArtId);
		.wait(1000);
		giveMeMyName;
		lookupArtifact("marketPlace",MarketPlaceId);
		linkArtifacts(ArtId,"out-1", MarketPlaceId);
		jia.randInt(X,5);
		+patience(X+1);
		connect.

// subscribe as soon as possible
		
+!subscribe : not running(true)
	<- .wait(1000);
		!subscribe.
		
+!subscribe : running(true) & own(Portfolio,_,_,_) & manage(Asset)
	<- 	subscribeForAssetInformation("EURONEXT - EURONEXT PARIS",Asset);
		buy(Asset, 1, Portfolio, "MKT"); // Initiate the market
		-manage(Asset);
		!subscribe.
		
+!subscribe : running(true)
	<- 	!trade.


// First step : guesstimate the price of the assets
+!trade : true 
	<- 	.wait(1000);
		!guesstimate.

+!guesstimate : indicators(_,_,Asset,_,_,_,Average,_,_,_)
	& not guesstimate(Asset,_)
	<-	.random(N); // return a random number between 0 and 1
		NN=Average +(N*3.0)-1.5;
		+guesstimate(Asset,NN);
		!guesstimate.

+!guesstimate : true <- !exchange.

// Second step : exchange the assets regarding the market and the portfolio

// If the agent is not yet a buyer
+!exchange : guesstimate(Asset,Price) // Buy if Price > market price
	& myName(Name)
	& indicators(_,Venue,Asset,_,_,_,_,_,_,_)
	& not existsBetter(Venue,"bid",Asset,Price)
	& own(Portfolio,_,"EURO",Money) 
	& not onMarket(_,Name, _, Venue, "bid",Asset,_,_) // he is not yet a buyer
	& Money>Price*1000
		<-	.concat("LMT ", Price,OPT);
			Q=Money/(Price*1000); // invest 1/4 of its stock in the asset
			jia.ceil(Q,N);
			buy(Asset, N, Portfolio, OPT);
			-guesstimate(Asset,Price);
			!exchange.
			
// else
+!exchange : guesstimate(Asset,Price) // Buy if Price > market price
	& myName(Name)
	& indicators(_,Venue,Asset,_,_,_,_,_,_,_)
	& not existsBetter(Venue,"bid",Asset,Price)
	& own(Portfolio,_,"EURO",Money) 
	& onMarket(Date,Name,Pf, Venue, "bid", Asset, Qty, Price2) // he is a buyer
	& Money>Price*1000
		<-	cancelOrder(Date,Pf,Venue,"bid",Asset,Qty,Price2);
			.concat("LMT ", Price,OPT);
			Q=Money/(Price*1000); // invest 1/4 of its stock in the asset
			jia.ceil(Q,N);
			buy(Asset, N, Portfolio, OPT);
			-guesstimate(Asset,Price);
			!exchange.
			
// If the agent is not yet a seller
+!exchange : guesstimate(Asset,Price) // Sell everything otherwise
	& myName(N)
	& indicators(_,Venue,Asset,_,_,_,_,_,_,_)
	& not onMarket(_,N, _, Venue, "ask",Asset,_,_) // He is not yet a seller
	& own(Portfolio,_,Asset,Q) 
		<-	.concat("LMT ", Price,OPT);
			sell(Asset, Q, Portfolio, OPT);
			-guesstimate(Asset,Price);
			!exchange.

//else
+!exchange : guesstimate(Asset,Price) // Sell everything otherwise
	& myName(N)
	& indicators(_,Venue,Asset,_,_,_,_,_,_,_)
	& onMarket(Date,N, Pf, Venue, "ask",Asset,Qty,Price2) // He is yet a seller
	& own(Portfolio,_,Asset,Q) 
		<-	cancelOrder(Date,Pf,Venue,"ask",Asset,Qty,Price2);
			.concat("LMT ", Price,OPT);
			sell(Asset, Q, Portfolio, OPT);
			-guesstimate(Asset,Price);
			!exchange.
			
			

+!exchange : guesstimate(Asset,Price)
	<- -guesstimate(Asset,Price);
			!exchange.

+!exchange : true <- !trade.

/*
// Trade randomly		
+!trade : not running(true)
	<-	.wait(1000);
		!trade.

// Buy if possible and not done yet and i dont have any asset yet
+!trade : running(true) & myName(N)
	& indicators(_,Venue,Asset,_,_,_,Average,DlAverage,_,_)
	& not onMarket(_,N, _, Venue, "bid",Asset,_,_) // I am not yet a buyer
	& Asset \== "EURO"
	& own(Portfolio,_,"EURO",Amount) // I have enough money
	& not own(Portfolio,_,Asset,_) // I dont have this asset yet
	& Amount >= Average*1000
	& patience(Z)
	& coin(1)
	<-	Max=Amount/(Average*1000);
		jia.randInt(X,Max); // previously Max-1
		.random(Y); // return a random number between 0 and 1
		Price=DlAverage+Y*2-1;
		Q=X+1; // We avoid to send a "Buy 0" order
		.concat("I want to buy ", Q, " ", Asset, " for ", Price, " Euro each one", Phrase);
		.print(Phrase);
		.concat("LMT ",Price,OPT);
		-+countdown(Z);
		buy(Asset, Q, Portfolio, OPT); // Ajouter les parametres 
		.wait(2000);
		jia.randInt(F,2);
		-+coin(F);
		!trade.
		
// Sell if possible and not done yet
+!trade : running(true) & myName(N) 
	& indicators(_,Venue,Asset,_,_,_,Average,DlAverage,_,_)
	& not onMarket(_,N, _, Venue, "ask",Asset,_,_) // I am not yet a seller
	& own(Portfolio,_,Asset,Amount)	// I have some asset to sell 
	& Asset \== "EURO"
	& patience(Z)
	& coin(0)
	<-	jia.randInt(X,Amount);
		.random(Y);
		Price=DlAverage+Y*2-1;
		Q=X+1; // We avoid to send a "Sell 0" order
		.concat("I want to sell ", Q, " ", Asset, " for ", Price, " Euro each one", Phrase);
		.print(Phrase);
		.concat("LMT ",Price,OPT);
		sell(Asset, Q, Portfolio, OPT); // Ajouter les parametres 
		.wait(2000);
		-+countdown(Z);
		jia.randInt(F,2);
		-+coin(F);
		!trade.

+!trade : running(true) & myName(N)	& timeloop(T) & countdown(Z) & Z<=0 & onMarket(Date,N,Pf, Venue, Side, Asset, Qty, Price)
	<- 	cancelOrder(Date,Pf,Venue,Side,Asset,Qty,Price);
		.wait(T);
		!trade.
		
+!trade : running(true) & countdown(T)
	<-  .wait(2000);
		-+countdown(T-1);
		jia.randInt(F,2);
		-+coin(F);
		!trade.
		*/
