// Agent zero_ethics_asset_manager in project ethicalHFT

{ include("$jacamoJar/templates/common-cartago.asl") }
{ include("$jacamoJar/templates/common-moise.asl") }
{ include("inc/situation_awareness.asl")}
{ include("inc/evaluation_process.asl")}

/* Initial beliefs and rules */

countdown(5).
timeloop(2500).

trade(X):- buy(X).
trade(X):- sell(X).

/* Initial goals */

!start.
!trade.
!subscribe.

/* Plans */

+!start : true 
	<-	.my_name(N);
		.concat("art_",N,ArtN);	
		makeArtifact(ArtN, "agentLink.AgentLinkArtifact", [N], ArtId);
		focus(ArtId);
		giveMeMyName;
		.wait(1000);
		.print("Hello World !");
		lookupArtifact("marketPlace",MarketPlaceId);
		linkArtifacts(ArtId,"out-1", MarketPlaceId);
		+patience(4);
		.random(X);
		+sensibility(X);
		connect.

// subscribe as soon as possible		
+!subscribe : not running(true)
	<- .wait(1000);
		!subscribe.
		
+!subscribe : running(true)
	<- 	subscribeForAssetInformation("EURONEXT - EURONEXT PARIS","AB SCIENCE");
		subscribeForAssetInformation("EURONEXT - EURONEXT PARIS","ACCOR").
		
// Trade randomly		
+!trade : not running(true)
	<-	.wait(1000);
		!trade.
		
// Sell if desired

// I am yet a seller
+!trade : running(true) & myName(N) 
	& onMarket(Date,N, Portfolio, Venue, "ask",Asset, Qty, P) // I am yet a seller
	& desire_eval(sell(Asset, Amount, Portfolio, Option), desired)
	& possible_eval(sell(Asset, Amount, Portfolio, Option), possible)
	& patience(Z)
	& timeloop(T)
	<-	cancelOrder(Date,Portfolio,Venue,"ask",Asset,Qty,P);
		.concat("I want to sell ", Amount, " ", Asset, " with options ", Option, Phrase);
		.print(Phrase);
		sell(Asset, Amount, Portfolio, Option); 
		.wait(T);
		-+countdown(Z);
		!trade.

+!trade : running(true)
	& desire_eval(sell(Asset, Amount, Portfolio, Option), desired)
	& possible_eval(sell(Asset, Amount, Portfolio, Option), possible)
	& patience(Z)
	& timeloop(T)
	<-	.concat("I want to sell ", Amount, " ", Asset, " with options ", Option, Phrase);
		.print(Phrase);
		sell(Asset, Amount, Portfolio, Option); 
		.wait(T);
		-+countdown(Z);
		!trade.

// Buy if desired
+!trade : running(true) & myName(N) 
	& onMarket(Date,N, Portfolio, Venue, "bid",Asset, Qty, P) // I am yet a buyer
	& desire_eval(buy(Asset, Amount, Portfolio, Option), desired)
	& possible_eval(buy(Asset, Amount, Portfolio, Option), possible)
	& patience(Z)
	& timeloop(T)
	<-	cancelOrder(Date,Portfolio,Venue,"bid",Asset,Qty,P);
		.concat("I want to buy ", Amount, " ", Asset, " with options ", Option, Phrase);
		.print(Phrase);
		buy(Asset, Amount, Portfolio, Option); 
		.wait(T);
		-+countdown(Z);
		!trade.	
		
+!trade : running(true)
	& desire_eval(buy(Asset, Amount, Portfolio, Option), desired)
	& possible_eval(buy(Asset, Amount, Portfolio, Option), possible)
	& patience(Z)
	& timeloop(T)
	<-	.concat("I want to buy ", Amount, " ", Asset, " with options ", Option, Phrase);
		.print(Phrase);
		buy(Asset, Amount, Portfolio, Option); 
		.wait(T);
		-+countdown(Z);
		!trade.	 

+!trade : running(true) & countdown(Z) & Z>0 & timeloop(T)
	<- .wait(T);
		-+countdown(Z-1);
		!trade.
		
+!trade : running(true) & myName(N)	& timeloop(T) & countdown(Z) & Z<=0 & onMarket(Date,N,Pf, Venue, Side, Asset, Qty, Price)
	<- 	cancelOrder(Date,Pf,Venue,Side,Asset,Qty,Price);
		.wait(T);
		!trade.
		
+!trade : running(true) & timeloop(T)
	<- .wait(T);
		!trade.
		