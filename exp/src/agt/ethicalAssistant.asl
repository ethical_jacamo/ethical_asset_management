// Agent ethicalAssistant in project ethicalHFT

{ include("$jacamoJar/templates/common-cartago.asl") }
{ include("$jacamoJar/templates/common-moise.asl") }

{ include("inc/ethical_assistant_arch.asl") }

//* Ethical preferences *//
prefEthics("aristotelian","perfectAct").
prefEthics("perfectAct","desireNR").
prefEthics("desireNR","dutyNR").

/* Initial beliefs and rules */

role("Default").