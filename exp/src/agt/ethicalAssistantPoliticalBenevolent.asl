// Agent ethicalAssistant in project ethicalHFT

{ include("$jacamoJar/templates/common-cartago.asl") }
{ include("$jacamoJar/templates/common-moise.asl") }

{ include("inc/ethical_assistant_arch.asl") }
{ include("inc/political_benevolence.asl") }

//* Ethical preferences *//
prefEthics("aristotelian","perfectAct").
prefEthics("perfectAct","desireNR").
prefEthics("desireNR","dutyNR").

//* Initial beliefs and rules *//
{ include("inc/common_belief_base.asl") }

role("Political benevolence").