// Internal action code for project ethicalHFT

package jia;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import jason.*;
import jason.asSemantics.*;
import jason.asSyntax.*;

public class differenceInMinutes extends DefaultInternalAction {

    @Override
    public Object execute(TransitionSystem ts, Unifier un, Term[] args) throws Exception {
    	final String sDate1;
    	final String sDate2;
    	if(args[0].isString() && args[1].isString())
    	{
    		sDate1 = args[0].toString();
    		sDate2 = args[1].toString();
    	}
    	else
    	{
    		return false;
    	}
    	// Wed Jun 01 17:55:14 CEST 2016  
    	String Date1= sDate1.substring(1, sDate1.length()-2);
    	String Date2= sDate2.substring(1, sDate2.length()-2);
    	DateFormat df = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy", Locale.ENGLISH);
    	Date d1 = df.parse(Date1);
    	Date d2 = df.parse(Date2);
    	long diff = (d1.getTime() - d2.getTime())/60000;
    	return un.unifies(args[2], new NumberTermImpl(diff));
    }
}
