// Internal action code for project ethicalHFT

package jia;

import java.util.Random;

import jason.*;
import jason.asSemantics.*;
import jason.asSyntax.*;

public class randInt extends DefaultInternalAction {

    @Override
    public Object execute(TransitionSystem ts, Unifier un, Term[] args) throws Exception {

        Random randomGenerator = new Random();
        final int max = (int)((NumberTerm)args[1]).solve();
        return un.unifies(args[0], new NumberTermImpl(randomGenerator.nextInt(max)));
    }
}
