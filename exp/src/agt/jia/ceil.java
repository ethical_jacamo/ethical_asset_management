// Internal action code for project ethicalHFT

package jia;

import jason.*;
import jason.asSemantics.*;
import jason.asSyntax.*;

public class ceil extends DefaultInternalAction {
	
	// .ceil(f,i) unifies i with the integer part of f
	
    @Override
    public Object execute(TransitionSystem ts, Unifier un, Term[] args) throws Exception {
        return un.unifies(args[1], new NumberTermImpl((int)((NumberTerm)args[0]).solve()));
    }
}
