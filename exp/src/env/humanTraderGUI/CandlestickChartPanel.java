package humanTraderGUI;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.Shape;
import java.util.LinkedList;

import javax.swing.JPanel;

import marketSim.model.AbstractAsset;


public class CandlestickChartPanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8972232943078938097L;

	protected Graphics2D drawImage;
	
	protected String assetName;
	
	protected int windowSize=1;
	protected int min=0,max=0, volumeMax=1;
	protected int margeIn=10; // pixels between the border and the chart
	
	protected LinkedList<Integer> open=new LinkedList<Integer>();
	protected LinkedList<Integer> close=new LinkedList<Integer>();
	protected LinkedList<Integer> bottom=new LinkedList<Integer>();
	protected LinkedList<Integer> up=new LinkedList<Integer>();
	protected LinkedList<Integer> average=new LinkedList<Integer>();
	protected LinkedList<Integer> doubleaverage=new LinkedList<Integer>();
	protected LinkedList<Integer> bollingerUp=new LinkedList<Integer>();
	protected LinkedList<Integer> bollingerDown=new LinkedList<Integer>();
	protected LinkedList<Integer> volume=new LinkedList<Integer>();
	
    public CandlestickChartPanel(String assetName)
    {
    	this.assetName=assetName;
		up.addLast(0);
		bottom.addLast(0);
		open.addLast(0);
		close.addLast(0);
		volume.addLast(0);
    }

    @Override
    public void paint(Graphics g) {
        drawImage = (Graphics2D) g;
        drawImage.setColor(Color.WHITE);
        drawImage.fillRect(0, 0, getWidth(), getHeight());
         
        // Compute the main 
        if(getHeight()<3*margeIn || getWidth()<3*margeIn) return;
        int abscissLineHeight = getHeight()-getHeight()/5;
        int leftLimit=margeIn;
        int rightLimit=getWidth()-margeIn;
        int bottomLimit= getHeight()-margeIn;
        int topLimit =margeIn;
        int stepWidth = (rightLimit - leftLimit)/windowSize;
        int HeightOfVolumeChart = bottomLimit - abscissLineHeight - margeIn;

        // Draw the dimensions
        drawImage.setColor(Color.BLACK);
        drawImage.draw((Shape) new Rectangle(leftLimit, abscissLineHeight, rightLimit-leftLimit, 1)); // Abscissa of the candlestick chart
        drawImage.draw((Shape) new Rectangle(leftLimit, topLimit, 1, abscissLineHeight-topLimit)); // Ordinate of the candlestick chart
        drawImage.draw((Shape) new Rectangle(leftLimit, bottomLimit, rightLimit-leftLimit, 1)); // Abscissa of the volume chart
        drawImage.draw((Shape) new Rectangle(leftLimit, abscissLineHeight+margeIn, 1, HeightOfVolumeChart)); // Ordinate of the volume chart

        // Draw the chart
        for(int i=0; i<volume.size(); i++)
        {
            drawImage.setColor(Color.RED);
            drawImage.fillRect(leftLimit+(i*stepWidth), bottomLimit+1-(volume.get(i)*HeightOfVolumeChart/volumeMax),
            		stepWidth, volume.get(i)*HeightOfVolumeChart/volumeMax); // draw each bar of the barchart
            
            drawImage.setColor(Color.BLACK);
            if(max-min>0)
            {
            	// Draw the local min/max line
            	drawImage.draw((Shape) new Rectangle((int) (leftLimit+(0.5+i)*stepWidth),
            		topLimit+(abscissLineHeight-topLimit)*(max-up.get(i))/(max-min),
            		1,
            		(abscissLineHeight-topLimit)*(up.get(i)-bottom.get(i))/(max-min)));
            
            	// Draw the open/close rectangle
            	if(open.get(i)<close.get(i))
            	{
                    drawImage.setColor(Color.GREEN);
                    drawImage.fillRect((int) (leftLimit+i*stepWidth),
                    		topLimit+(abscissLineHeight-topLimit)*(max-close.get(i))/(max-min),
                    		stepWidth,
                    		(abscissLineHeight-topLimit)*(close.get(i)-open.get(i))/(max-min));
            	}
            	if(open.get(i)>close.get(i))
            	{
            		drawImage.setColor(Color.RED);
                    drawImage.fillRect((int) (leftLimit+i*stepWidth),
                    		topLimit+(abscissLineHeight-topLimit)*(max-open.get(i))/(max-min),
                    		stepWidth,
                    		(abscissLineHeight-topLimit)*(open.get(i)-close.get(i))/(max-min));
            	}
            }
        }
        for(int i=1; i<volume.size()-1; i++)
        {
            if(max-min>0)
            {
            	// System.out.println("Bdown=" + bollingerDown.get(i)+ " Bup=" + bollingerUp.get(i) + " average=" +average.get(i) + " 2avr=" + doubleaverage.get(i));
            	// draw the lowest bollinger
            	drawImage.setColor(Color.YELLOW);
            	drawImage.drawLine((int) (leftLimit+(i-0.5)*stepWidth),
            			topLimit+(abscissLineHeight-topLimit)*(max-bollingerDown.get(i-1))/(max-min), 
            			(int) (leftLimit+(i+0.5)*stepWidth),
            			topLimit+(abscissLineHeight-topLimit)*(max-bollingerDown.get(i))/(max-min));
            	// draw the highest bollinger
            	drawImage.setColor(Color.ORANGE);
            	drawImage.drawLine((int) (leftLimit+(i-0.5)*stepWidth),
            			topLimit+(abscissLineHeight-topLimit)*(max-bollingerUp.get(i-1))/(max-min), 
            			(int) (leftLimit+(i+0.5)*stepWidth),
            			topLimit+(abscissLineHeight-topLimit)*(max-bollingerUp.get(i))/(max-min));
            	// draw the average
            	drawImage.setColor(Color.MAGENTA);
            	drawImage.drawLine((int) (leftLimit+(i-0.5)*stepWidth),
            			topLimit+(abscissLineHeight-topLimit)*(max-average.get(i-1))/(max-min), 
            			(int) (leftLimit+(i+0.5)*stepWidth),
            			topLimit+(abscissLineHeight-topLimit)*(max-average.get(i))/(max-min));
            	// draw the long-term average
            	drawImage.setColor(Color.DARK_GRAY);
            	drawImage.drawLine((int) (leftLimit+(i-0.5)*stepWidth),
            			topLimit+(abscissLineHeight-topLimit)*(max-doubleaverage.get(i-1))/(max-min), 
            			(int) (leftLimit+(i+0.5)*stepWidth),
            			topLimit+(abscissLineHeight-topLimit)*(max-doubleaverage.get(i))/(max-min));
            }
        }
    }

    public void updateGraphics(int length,int width)
    {
        drawImage.setColor(Color.black);
        drawImage.drawRect(100, 150, length, width);
        repaint();
    }
    
    public void setDuration(int windowSize)
    {
    	this.windowSize=windowSize;
    }

    public void addOrder(int price, int volume)
    {
    	// System.out.println("Ordre de " + volume + " pour " + price + "€");
		this.volume.set(this.volume.size()-1,this.volume.getLast()+volume);
		if(this.volume.getLast()>volumeMax) volumeMax=this.volume.getLast();// updates the volumeMax if needed
		
		if(price<min || min == 0) min = price;
		if(price>max) max=price;
		if(up.getLast()<price) up.set(up.size()-1, price);// updates the local max if needed
		if(bottom.getLast()>price || bottom.getLast()==0) bottom.set(bottom.size()-1, price);// updates the local min if needed
		
		if(open.getLast()==0) open.set(open.size()-1, price); // updates the open if it is the first order
		
		close.set(close.size()-1, price); // always updates the close value
			
		repaint();
    }
    
    public String getAssetName()
    {
    	return assetName;
    }

	public void updateIndicator(float x_close, long v,
			float intensity, float average, float doubleLengthAverage,
			float bollingerUp2, float bollingerDow) 
	{
		//close.set(close.size()-1, (int) x_close*1000);
		close.addLast(0);
		//volume.set(volume.size()-1,(int) v);
		volume.addLast(0);
		doubleaverage.addLast((int)(doubleLengthAverage*1000.0));
		this.average.addLast((int)(average*1000.0));
		bollingerUp.addLast((int)(bollingerUp2*1000.0));
		bollingerDown.addLast((int)(bollingerDow*1000.0));
		if((int)(bollingerDow*1000.0)<min) min = (int) (bollingerDow*1000.0);
		if((int)(bollingerUp2*1000.0)>max) max = (int) (bollingerUp2*1000.0);
		up.addLast(0);
		bottom.addLast(0);
		open.addLast(0);
		if(close.size()>windowSize) // The window is full : we delete the first element
		{
			close.pollFirst();
			open.pollFirst();
			volume.pollFirst();
			doubleaverage.pollFirst();
			this.average.pollFirst();
			bollingerUp.pollFirst();
			bollingerDown.pollFirst();
			up.pollFirst();
			bottom.pollFirst();
		}
		repaint();
	}
}
