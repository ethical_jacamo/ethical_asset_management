/**
 * 
 */
package humanTraderGUI;

import java.awt.Component;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

import marketSim.model.AbstractAsset;
import net.infonode.docking.View;
import net.infonode.docking.util.DockingUtil;
/**
 * 
 * @author Nicolas
 *
 */
public class MainTradingView extends View implements ActionListener{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3754544854209111454L;
	/**
	 * 
	 */

	//private String agent=null;
	private JComponent component=null;
	
	JDialog popupMenu;
	
	HumanTraderGUI gui;
	
	// Graphical elements
    protected HashMap<String,LinkedList<Component>> assetLine = new HashMap<String,LinkedList<Component>>();
    protected LinkedList<String> assetList;

    // nameLabel=new JLabel("<html>Name of the agent : <font color='green'>▲ +1.5</font> <font color='red'>▼ -0.4</font></html>");

	
	/**
	 * Constructor inherited from net.infonode.docking.View
	 * @param title the title of the view pane
	 * @param icon the icon of the pane
	 * @param component the component to put into the view
	 */
	public MainTradingView(Icon icon, JComponent compo, HumanTraderGUI gui)
	{
		super("Portfolio", icon, compo);
		this.gui=gui;
		
	    component=compo;
	    component.setLayout(new GridLayout(1,9));
	    component.add(new JLabel("Asset"));
	    component.add(new JLabel("Market Price"));
	    component.add(new JLabel("Volume/minute"));
	    component.add(new JLabel("Intensity"));
	    component.add(new JLabel("Average price"));
	    component.add(new JLabel("In Portfolio"));
	    component.add(new JLabel(""));
	    component.add(new JLabel(""));
	    component.add(new JLabel(""));
	    compo.setSize(500, 400);
	}
	
	public void setAssetList(LinkedList<String> assetList)
	{
	    assetLine = new HashMap<String,LinkedList<Component>>();
	    //component = new JPanel();
	    component.removeAll();
	    component.setLayout(new GridLayout(assetList.size()+1,9));
	    component.add(new JLabel("Asset"));
	    component.add(new JLabel("Market Price"));
	    component.add(new JLabel("Volume/minute"));
	    component.add(new JLabel("Intensity"));
	    component.add(new JLabel("Average price"));
	    component.add(new JLabel("In Portfolio"));
	    component.add(new JLabel(""));
	    component.add(new JLabel(""));
	    component.add(new JLabel(""));
	    
	    for(int i=0; i<assetList.size(); i++)
	    {
	    	//if(assetList.get(i).equals("EURO")) continue;
	    	
	    	LinkedList<Component> line = new LinkedList<Component>();
	    	line.addLast(new JLabel(assetList.get(i)));
	    	line.addLast(new JLabel("-"));
	    	line.addLast(new JLabel("-"));
	    	line.addLast(new JLabel("-"));
	    	line.addLast(new JLabel("-"));
	    	line.addLast(new JLabel("-"));
	    	line.addLast(new JButton("Buy"));
	    	((JButton) line.get(6)).addActionListener(this);
	    	line.addLast(new JButton("Sell"));
	    	((JButton) line.get(7)).addActionListener(this);
	    	line.addLast(new JButton("Chart"));
	    	((JButton) line.get(8)).addActionListener(this);
	    
	    	assetLine.put(assetList.get(i),line);
    	
	    	for(int j=0; j<9; j++)
	    		component.add(assetLine.get(assetList.get(i)).get(j));
	    }
	}
	
	public void updateIndicator(AbstractAsset asset, float x_close,
			long v, float intensity, float average, float doubleLengthAverage,
			float bollingerUp, float bollingerDow)
	{
		((JLabel) assetLine.get(asset.getName()).get(1)).setText(Float.toString(x_close));
		((JLabel) assetLine.get(asset.getName()).get(2)).setText(Long.toString(v));
		((JLabel) assetLine.get(asset.getName()).get(3)).setText(Float.toString(intensity));
		((JLabel) assetLine.get(asset.getName()).get(4)).setText(Float.toString(average));
	}
	
	public void updateAssetInPortfolio(AbstractAsset asset, int qty)
	{
		if(assetLine.containsKey(asset.getName()))
			((JLabel) assetLine.get(asset.getName()).get(5)).setText(Integer.toString(qty));
	}

	@Override
	public void actionPerformed(ActionEvent e) 
	{
		Object source = e.getSource();
		Iterator<String> it = assetLine.keySet().iterator();
		for(int i=0; i < assetLine.keySet().size(); i++)
		{
			final String asset= it.next();
			if(source == assetLine.get(asset).get(6)) // Buy button pressed
			{
				SwingUtilities.invokeLater(new Runnable() {
    				@Override
    				public void run() {
    					try {
    						UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
    					} catch(Exception ex) {
    						ex.printStackTrace();
    					}
    					JDialog menu=new BuyerMenu(asset, gui);
    					menu.setLocation(gui.jFrame.getLocation().x+20,gui.jFrame.getLocation().y+20);
       					menu.setVisible(true);
    				}
    			});
			}
			if(source == assetLine.get(asset).get(7)) // Sell button pressed
			{
				SwingUtilities.invokeLater(new Runnable() {
    				@Override
    				public void run() {
    					try {
    						UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
    					} catch(Exception ex) {
    						ex.printStackTrace();
    					}
    					JDialog menu=new SellerMenu(asset, gui);
    					menu.setLocation(gui.jFrame.getLocation().x+20,gui.jFrame.getLocation().y+20);
       					menu.setVisible(true);
    				}
    			});
			}
			if(source == assetLine.get(asset).get(8)) // Chart button pressed
			{
				// Create the window
				CandlestickChartPanel chart = new CandlestickChartPanel(asset);
				chart.setDuration(200); // Width = 200 minutes
				AssetChartView chartView= new AssetChartView(asset, new ImageIcon("./img/assetIcon.png"), chart );
				DockingUtil.addWindow(chartView, gui.getRootWindow());
				gui.addView(chartView);
			}
		}				
	}

	
}