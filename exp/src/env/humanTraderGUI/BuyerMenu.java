package humanTraderGUI;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

public class BuyerMenu extends JDialog implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6906790995913296071L;
	JPanel panel;
	JTextField textField;
	HumanTraderGUI gui;
	String asset;
	JButton okButton;
	JButton cancelButton;
	
	public BuyerMenu(String asset, HumanTraderGUI gui)
	{
		this.gui=gui;
		this.asset=asset;
		
		panel = new JPanel(new GridLayout(2,2));
		JLabel qtyLbl =new JLabel("Quantity : ");
		qtyLbl.setHorizontalAlignment(SwingConstants.RIGHT);
        panel.add(qtyLbl);
        textField = new JTextField();
		panel.add(textField);
		okButton = new JButton("OK");
		okButton.addActionListener(this);
		cancelButton = new JButton("Cancel");
		cancelButton.addActionListener(this);
		panel.add(okButton);
		panel.add(cancelButton);
		add(panel);
		setSize(200, 80);
		setTitle("Buy "+ asset);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		Object source = e.getSource();
		if(source == okButton)
		{
			gui.buy(asset, Integer.parseInt(textField.getText()));
			this.dispose();
		}
		if(source == cancelButton)
		{
			this.dispose();
		}
	}
	
	

}
