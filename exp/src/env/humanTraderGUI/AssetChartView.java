/**
 * 
 */
package humanTraderGUI;

import java.awt.Component;

import javax.swing.Icon;

import cartago.tools.GUIArtifact;
import net.infonode.docking.View;

/**
 * @author Nicolas
 *
 */
public class AssetChartView extends View{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2772299171246973709L;
	
	protected String name;

	/**
	 * @param arg0 
	 * @param arg1
	 * @param arg2
	 */
	public AssetChartView(String arg0, Icon arg1, Component arg2) {
		super(arg0, arg1, arg2);
		name = arg0;
	}

	public String getName()
	{
		return name;
	}
}
