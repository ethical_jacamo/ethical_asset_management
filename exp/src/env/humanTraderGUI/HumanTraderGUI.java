/**
 * 
 */
package humanTraderGUI;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
// TODO Implement serialization and deserialization of the ui
/*import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;*/
import java.util.LinkedList;

import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTree;
import javax.swing.text.DefaultCaret;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;

import cartago.ArtifactId;
import cartago.INTERNAL_OPERATION;
import cartago.LINK;
import cartago.OPERATION;
import cartago.tools.GUIArtifact;
import marketSim.model.AbstractAsset;
import marketSim.model.AbstractTradingVenue;
import marketSim.model.Order;
import marketSim.model.Portfolio;
import marketSim.view.AgentView;
import net.infonode.docking.*;
import net.infonode.docking.properties.RootWindowProperties;
import net.infonode.docking.util.DockingUtil;
import net.infonode.docking.util.PropertiesUtil;
import net.infonode.docking.util.ViewMap;

/**
 * This view mix Cartago, infonode and swing components to display a user interface with dockable windows.
 * @author Nicolas
 */
public class HumanTraderGUI extends GUIArtifact
{	
	private static final String CONSOLE = "console";
	private static final String TESTASSET = "testAsset"; // TODO remove after the test

	private ViewMap viewMap;
	private LinkedList<View> temporaryViews;
	
	private JTree simTree;
	private MainTradingView mainTradingView;

	private JTextArea console;
	private RootWindow rootWindow;
	protected JFrame jFrame;
	protected Date date;
	protected JLabel dateLabel;
	
	protected HashMap<String,JMenuItem> assistants = new HashMap<String,JMenuItem>();
	protected HashMap<String,String> contacts = new HashMap<String,String>(); // Role/name
	protected String selectedAssistant = "Default";
	protected LinkedList<String> assetList;
		
	// Swing components
	JMenu menuHumanTraderGUI, optionsMenu, assistantSubmenu;
	JMenuItem quitButton;
	
	public void setup()
	{
		this.jFrame= new JFrame();
		this.console = new JTextArea();
		DefaultCaret caret = (DefaultCaret)this.console.getCaret();
		caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
		this.temporaryViews= new LinkedList<View>();
		build();
	    this.jFrame.setVisible(true);
	}
	
	/**
	 * Build the main window
	 */
	private void build()
	{
		this.jFrame.setTitle("Human Trader Interface");	
		this.jFrame.setSize(1000,500);
		this.jFrame.setLocationRelativeTo(null);
		this.jFrame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		
		JMenuBar menuBar = new JMenuBar();
		
		// Menu "Simulator"
		menuHumanTraderGUI = new JMenu("Trading Interface");
		quitButton = new JMenuItem("Quit");
		menuHumanTraderGUI.add(quitButton);
		
		menuBar.add(menuHumanTraderGUI);
		
		// Menu "Options"
		optionsMenu = new JMenu("Options");
			assistantSubmenu = new JMenu("assistant");
			optionsMenu.add(assistantSubmenu);
		menuBar.add(optionsMenu);
		
		dateLabel = new JLabel();
		date = new Date();
		dateLabel.setText(date.toString());
        menuBar.add(Box.createHorizontalGlue());
		menuBar.add(dateLabel);
		
		this.jFrame.setJMenuBar(menuBar);
		
		this.viewMap=new ViewMap();
		
		View consoleView= new View("Console", new ImageIcon("./img/consoleIcon.png"), createViewComponent(CONSOLE));
		this.viewMap.addView(2, consoleView);
		
		this.rootWindow=DockingUtil.createRootWindow(this.viewMap,true);
		
		// Enable title bar style
		RootWindowProperties titleBarStyleProperties = PropertiesUtil.createTitleBarStyleRootWindowProperties();
		rootWindow.getRootWindowProperties().addSuperObject(titleBarStyleProperties);
		
		this.jFrame.setContentPane(this.rootWindow);

	    linkActionEventToOp(this.quitButton,"closed");
	}

	/**
	 * Create a component of the main window
	 * @param text name of the component's type
	 * @return the created component
	 */
	private Component createViewComponent(String text)
	{
		if(text.equals(CONSOLE)) return new JScrollPane(this.console);

		return new JScrollPane(new JTextArea());
	}
	
	/**
	 * Print a text into the console of the main window
	 * @param text
	 */
	@LINK
	void print(String text)
	{
		try
		{
			this.console.append(text);
		}
		catch(Exception e)
		{
			System.out.println("ERROR : unable to print '" + text + "' in console.");
		}
	}
	
	/**
	 * Add a Portfolio to its owner in the tree
	 */
	public void addPortfolio()
	{
		// TODO implements this method to put a Portfolio in the tree
	}

	@INTERNAL_OPERATION
	void closed(ActionEvent ev)
	{
		int confirm = JOptionPane.showOptionDialog(
	             null, "Are You Sure to Close Application?", 
	             "Exit Confirmation", JOptionPane.YES_NO_OPTION, 
	             JOptionPane.QUESTION_MESSAGE, null, null, null);
	    if (confirm == 0) 
	    {
	    	signal("closed");
	    	jFrame.dispose();
	    	// TODO kill the artifact
	    }
	}
	
	/**
	 * Linked operation called by the clock artifact to update the date
	 * @param newDate the new date
	 */
	@LINK
	void updateDate(Date newDate) // TODO Create an interface "timeSubscriber" having this method
	{
		date = newDate;
		dateLabel.setText(date.toString());
	}
	
	@OPERATION
	void connectGUI(String name,String role)
	{
		JCheckBoxMenuItem button = new JCheckBoxMenuItem(role);
		assistants.put(role, button);
		contacts.put(role, name);
		linkActionEventToOp(button,"changeAssistant");
		if(role.equalsIgnoreCase(selectedAssistant)) button.setSelected(true);
		assistantSubmenu.add(button);
		this.jFrame.validate();
		this.jFrame.repaint();
	}
	
	@INTERNAL_OPERATION
	void changeAssistant(ActionEvent ev)
	{
		String exSelectedAssistant=selectedAssistant;
		Iterator<String> it = assistants.keySet().iterator();
		while(it.hasNext())
		{
			String cursor = it.next();
			if(cursor.equals(exSelectedAssistant))
				assistants.get(cursor).setSelected(false);
			else if (assistants.get(cursor).isSelected()) 
			{
				selectedAssistant=cursor;
			}
		}
	}
	
	@LINK
	void setRunning()
	{
		print("The simulation is launched.\n");
	}
	
	/**
	 * Add an asset to the tree 
	 * @param artifact
	 */
	@LINK
	public void addAsset(String assetName)
	{
		// TODO Verify that the asset is not yet in the tree.
		DefaultMutableTreeNode root = (DefaultMutableTreeNode) simTree.getModel().getRoot();
		((DefaultMutableTreeNode) root.getChildAt(2)).add(new DefaultMutableTreeNode(assetName));
		((DefaultTreeModel) simTree.getModel()).reload(root);
	}
	
	@LINK
	public void setAssetList(LinkedList<String>assetList)
	{
		this.assetList=assetList;		
	}
	
	@LINK
	public void signalIndicatorChanges(Date dateOfEnd,
			AbstractTradingVenue venue, AbstractAsset asset, float x_close,
			long v, float intensity, float average, float doubleLengthAverage,
			float bollingerUp, float bollingerDow)
	{
		mainTradingView.updateIndicator(asset, x_close, v, intensity, average, doubleLengthAverage, bollingerUp, bollingerDow);
		
		// Update the charts
		for(int i=0; i<temporaryViews.size();i++)
		{
			if(AssetChartView.class.isInstance(temporaryViews.get(i)))
			{
				if(temporaryViews.get(i).getName().compareTo(asset.getName())== 0)
				{
					try{
						((CandlestickChartPanel)temporaryViews.get(i).getComponent()).updateIndicator(
								x_close, v, intensity, average, doubleLengthAverage, bollingerUp, bollingerDow);
					}
					catch(Exception e)
					{
						// TODO
					}
				}
			}
		}
		
	}
	
	@LINK
	public void updatePorfolios(ArrayList<Portfolio> pf)
	{
		print("Called and " + pf.size() + " named " + pf.get(0).getName() + "\n");
		for(int i=0; i<pf.size(); i++)
		{
			mainTradingView= new MainTradingView(new ImageIcon("./img/tradeIcon.png"), new JPanel(), this);
			mainTradingView.getViewProperties().setTitle(pf.get(i).getName());
			mainTradingView.setAssetList(assetList);
			for(int a=0; a<pf.get(i).getAssets().size();a++)
				mainTradingView.updateAssetInPortfolio(pf.get(i).getAssets().get(a), 
						pf.get(i).getVallueOfAssetByName(pf.get(i).getAssets().get(a).getName()));
			this.viewMap.addView(1, mainTradingView);
			DockingUtil.addWindow(mainTradingView, this.rootWindow);
		}
	}
	
	@LINK
	public void signalExecutionOrder(AbstractTradingVenue venue, Order newOrder, Order oldOrder, int quantity, Date date)
	{
		for(int i=0; i<temporaryViews.size();i++)
		{
			if(AssetChartView.class.isInstance(temporaryViews.get(i)))
			{
				if(temporaryViews.get(i).getName().compareTo(newOrder.getAsset().getName())== 0)
				{
					try{
						((CandlestickChartPanel)temporaryViews.get(i).getComponent()).addOrder((int)(newOrder.getLimit()*1000), quantity);
					}
					catch(Exception e)
					{
						// TODO
					}
				}
			}
		}
	}
	
	@LINK
	public void signalAdditionOrder(AbstractTradingVenue venue, Order order)
	{
		// TODO
	}
	
	@LINK
	public void signalCancelationOrder(AbstractTradingVenue venue, Order order)
	{
		// TODO
	}
	
	@OPERATION
	void test()
	{
		print("test\n");
	}
	
	@OPERATION
	void buy(String asset, int qty)
	{
		print("Order to buy " + qty + " " + asset + "\n");
	}
	
	@OPERATION
	void sell(String asset, int qty)
	{
		print("Order to sell " + qty + " " + asset + "\n");
	}
	
	void addView(View view)
	{
		this.viewMap.addView(2, view);
		temporaryViews.add(view);
	}
	
	RootWindow getRootWindow()
	{
		return rootWindow;
	}
}