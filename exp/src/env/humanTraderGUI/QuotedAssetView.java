/**
 * 
 */
package humanTraderGUI;

import java.awt.Button;
import java.awt.Desktop;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SpringLayout;

import net.infonode.docking.View;
/**
 * 
 * @author Nicolas
 *
 */
public class QuotedAssetView extends View {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3754544854209111454L;
	/**
	 * 
	 */

	private String agent=null;
	private JComponent component=null;
	
	// Graphical elements
	private JLabel nameLabel = null;
	private JTextField nameTextField = null;
	private JButton webInterfaceButton=null;
	private BufferedImage image;

	
	/**
	 * Constructor inherited from net.infonode.docking.View
	 * @param title the title of the view pane
	 * @param icon the icon of the pane
	 * @param component the component to put into the view
	 */
	public QuotedAssetView(Icon icon, JComponent compo, final String agent)
	{
		super(agent, icon, compo);
		/*BufferedImage myPicture = null;
		try {
			myPicture = ImageIO.read(new File("output.png"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		JLabel picLabel = new JLabel(new ImageIcon(myPicture));
		SpringLayout layout = new SpringLayout();
		compo.setLayout(layout);
		nameLabel=new JLabel("<html>Name of the agent : <font color='green'>▲ +1.5</font> <font color='red'>▼ -0.4</font></html>");
		//compo.add(picLabel);
		compo.add(nameLabel);
		compo.repaint();*/
	    
		compo.setLayout(new GridLayout(3,2));
        compo.add(new Button("<html>Name of the agent : <font color='green'>▲ +1.5</font> <font color='red'>▼ -0.4</font></html>"));
        compo.add(new Button("2"));
        compo.add(new Button("3"));
        compo.add(new Button("4"));
        compo.add(new Button("5"));
        compo.add(new Button("6"));
        
	}
}