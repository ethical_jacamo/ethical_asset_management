/**
 * 
 */
package agentLink;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import marketSim.model.AbstractAsset;
import marketSim.model.AbstractTradingVenue;
import marketSim.model.Order;
import marketSim.model.Portfolio;
import marketSim.model.Side;
import cartago.ARTIFACT_INFO;
import cartago.Artifact;
import cartago.ArtifactId;
import cartago.INTERNAL_OPERATION;
import cartago.LINK;
import cartago.OPERATION;
import cartago.OUTPORT;
import cartago.OperationException;


/**
 * @author Nicolas
 *
 */

@ARTIFACT_INFO(outports={ @OUTPORT(name = "out-1")	})
public class AgentLinkArtifact extends Artifact 
{
	protected String agentName;
	ArrayList<Portfolio> portfolios=new ArrayList<Portfolio>();
	PrintWriter imagesLog;
	boolean logImages=false;
	Date currentTime=null;
	
	@OPERATION
    void init(String name) 
	{
       agentName=name;
       defineObsProperty("running", false);
       defineObsProperty("changedPortfolio", false);
       try {
		imagesLog=new PrintWriter("./out/data/" + name +"ImagesLog.log","UTF-8");
       } catch (FileNotFoundException | UnsupportedEncodingException e) {
    	   // TODO Auto-generated catch block
    	   e.printStackTrace();
       }

	}
	
	@OPERATION 
	void connect()
	{
		try {
			execLinkedOp("out-1","connect",getId(), agentName);
		} catch (OperationException e) {
			e.printStackTrace();
		}
	}
	
	@LINK
	void updateBelief(String belief, Object value)
	{
		getObsProperty(belief).updateValue(value);
	}
	
	@LINK
	void updatePorfolios(ArrayList<Portfolio> stackPortfolios)
	{
		signal("erasePortfoliosBeliefs");
		for(int i=0; i<stackPortfolios.size(); i++)
		{
			Portfolio p=stackPortfolios.get(0);
			for(int j=0; j<p.size(); j++)
				addInPortfolio(p.getName(),p.getBroker(),p.get(j),p.getValueForAsset(p.get(j)));
		}		
	}
	
	@OPERATION
	void print(String msg)
	{
		try 
		{
			ArtifactId guiId=lookupArtifact("gui");
			execLinkedOp(guiId, "print", msg);
		}
		catch (OperationException e)
		{
			System.out.print(msg);
		}

	}

	@OPERATION
	void sell(String asset, int amount, String pfName, String options)
	{
		try {
			execLinkedOp("out-1","sell", getId(), asset, amount, pfName, options);
		} catch (OperationException e) {
			e.printStackTrace();
		}
	}
	
	@OPERATION
	void buy(String asset, int amount, String pfName, String options)
	{
		try {
			execLinkedOp("out-1","buy", getId(), asset, amount, pfName, options);
		} catch (OperationException e) {
			e.printStackTrace();
		}
	}
	
	@OPERATION
	void cancelOrder(String sDate, String pfName, String venue, String sSide, String asset, int amount, double price)
	{
    	DateFormat df = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy", Locale.ENGLISH);
    	Date date = null;
    	try {
			date = df.parse(sDate);
		} catch (ParseException e1) {
			e1.printStackTrace();
		}
    	Side side=Side.ASK;
    	if(sSide=="bid") side=Side.BID;
		try {
			execLinkedOp("out-1","cancelOrder", getId(), date, pfName, venue, side, asset, amount, (int) (price*1000.0));
		} catch (OperationException e) {
			e.printStackTrace();
		}
	}
	
	@INTERNAL_OPERATION
	void addInPortfolio(String pName, String bName, AbstractAsset a, int quantity)
	{
		signal("addInPortfolios",pName, bName, a.getName(), quantity);
		for(int i=0; i<portfolios.size(); i++) // Try to find the portfolio
		{
			if(portfolios.get(i).getName().compareTo(pName)==0)
			{
				portfolios.get(i).addAssets(a, quantity);
				return;
			}
		}
		// If not found : create the portfolio
		Portfolio p = new Portfolio(pName, agentName, bName);
		p.addAssets(a, quantity);
		portfolios.add(p);
	}
	
	@OPERATION
	void subscribeForAssetInformation(String venue, String asset)
	{
		try {
			execLinkedOp("out-1","subscribe", getId(), asset, venue);
		} catch (OperationException e) {
			e.printStackTrace();
		}
	}
	
	@LINK
	void signalAdditionOrder(AbstractTradingVenue venue, Order order)
	{
		String side="bid";
		if(order.getSide()==Side.ASK) side="ask";
		signal("additionOrder", 
				order.getPlacementDate().toString(), 
				order.getPortfolio().getHolder(),
				order.getPortfolio().getName(), 
				venue.getName(), 
				side, 
				order.getAsset().getName(), 
				order.getQuantity(),
				order.getLimit());
	}
	
	@LINK
	void signalExecutionOrder(AbstractTradingVenue venue, Order newOrder, Order oldOrder, int quantity, Date date)
	{
		String side="bid";
		if(newOrder.getSide()==Side.ASK) side="ask"; 
		// Add belief for the new order
		signal("executionOrder", 
				date.toString(), 
				newOrder.getPortfolio().getHolder(),
				newOrder.getPortfolio().getName(), 
				venue.getName(), 
				side, 
				newOrder.getAsset().getName(), 
				quantity,
				newOrder.getLimit());
		// Check to update or remove the old order
		side="bid";
		if(oldOrder.getSide()==Side.ASK) 
		{
			side="ask"; 
		}
		// We remove the old belief about this order
		signal("deletionOrder",//DateOfPlacement, Agent, Portfolio, Venue,Side,Asset,Amount
				oldOrder.getPlacementDate().toString(), 
				oldOrder.getPortfolio().getHolder(),
				oldOrder.getPortfolio().getName(), 
				venue.getName(), 
				side, 
				oldOrder.getAsset().getName(), 
				oldOrder.getQuantity(),
				oldOrder.getLimit());
		if(quantity!=oldOrder.getQuantity()) // We place a new belief if the old order is not fully executed
			signal("updateOrder",
					oldOrder.getPlacementDate().toString(), 
					oldOrder.getPortfolio().getHolder(),
					oldOrder.getPortfolio().getName(), 
					venue.getName(), 
					side, 
					oldOrder.getAsset().getName(), 
					oldOrder.getQuantity()-quantity,
					oldOrder.getLimit());
		// Add belief for the execution of the old order
		signal("executionOrder", 
				date.toString(), 
				oldOrder.getPortfolio().getHolder(),
				oldOrder.getPortfolio().getName(), 
				
				venue.getName(), 
				side, 
				oldOrder.getAsset().getName(), 
				quantity,
				oldOrder.getLimit());
	}
	
	@LINK
	void signalIndicatorChanges(Date dateOfEnd,
			AbstractTradingVenue venue, AbstractAsset asset, float x_close,
			long v, float intensity, float average, float doubleLengthAverage,
			float bollingerUp, float bollingerDown) 
	{
		signal("updateIndicator", 
				dateOfEnd.toString(),
				venue.getName(),
				asset.getName(),
				x_close,
				v,
				intensity,
				average,
				doubleLengthAverage,
				bollingerUp,
				bollingerDown);
		currentTime=dateOfEnd;
	}
	
	@LINK
	void signalCancelationOrder(AbstractTradingVenue venue, Order order) 
	{
		String side="bid";
		if(order.getSide()==Side.ASK) side="ask";
		signal("updateCanceledOrder", 
				order.getPlacementDate().toString(), 
				order.getPortfolio().getHolder(),
				order.getPortfolio().getName(), 
				venue.getName(), 
				side, 
				order.getAsset().getName(), 
				order.getQuantity(),
				order.getLimit());
	}

	
	@OPERATION
	void giveMeMyName()
	{
		signal("giveName", agentName);
	}
	
	@OPERATION
	void changeImageof(String Agent, String moralSet, int valuePos, int valueNeg)
	{
		imagesLog.println(currentTime + " imageOf("+Agent+","+moralSet+","+valuePos+ ","+ valueNeg +")");
		imagesLog.flush();
	}
	
	@LINK
	void updateImagesLog()
	{
		logImages=true;
	}
}
