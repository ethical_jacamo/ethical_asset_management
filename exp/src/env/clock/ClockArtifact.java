/**
 * 
 */
package clock;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import cartago.Artifact;
import cartago.ArtifactId;
import cartago.INTERNAL_OPERATION;
import cartago.LINK;
import cartago.OPERATION;
import cartago.OperationException;

/**
 * @author Nicolas
 * Inspired from example 06 of the document "Cartago By Example"
 */
public class ClockArtifact extends Artifact 
{
	boolean counting;
	final static long TICK_TIME = 100;
	int increment=1; // multiplier of time progression
	
	int pastTime=0;
	int duration=200;
	
	int speed = 1;
	ArrayList<ArtifactId> subscribers = new ArrayList<ArtifactId>();
	
	Calendar calendar=Calendar.getInstance();
	Date end=new Date();

	void init()
	{
		counting = false;
	}
	
	/**
	 * Linked operation used by an other artifact to be added in the diffusion list
	 * @param id ArtifactId of the subscriber
	 */
	@LINK
	void subscribe(ArtifactId id)
	{
		if(subscribers.contains(id)) return;
		subscribers.add(id);
		try {
			execLinkedOp(id,"updateDate", calendar.getTime());
		} catch (OperationException e) {
			e.printStackTrace();
		}
	}
	
	@OPERATION void start()
	{
		if(!counting)
		{
			counting = true;
			execInternalOp("count");
			end= new Date(calendar.getTime().getTime() + 60000*duration);
		}
		else
			failed("already_counting");
	}
	
	@OPERATION void stop()
	{
		counting=false;
	}
	
	@INTERNAL_OPERATION void count()
	{
		while(counting)
		{
			signal("tick");
			await_time(TICK_TIME);
			calendar.add(Calendar.MILLISECOND, ((int) TICK_TIME)*increment);
			diffuse();
			if(calendar.getTime().getTime() > end.getTime()) System.exit(0);
		}
	}
	
	@LINK
	void setSpeed(int s)
	{
		speed=s;
	}
	
	@LINK
	void setDate(Date date)
	{
		calendar.setTime(date);
	}
	
	@LINK
	void setDuration(int duration)
	{
		this.duration=duration;
	}

	
	@OPERATION
	void diffuse()
	{
		for(int i=0; i<subscribers.size(); i++)
		{
			try {
				execLinkedOp(subscribers.get(i),"updateDate", calendar.getTime());
			} catch (OperationException e) {
				e.printStackTrace();
			}
		}
	}
}
