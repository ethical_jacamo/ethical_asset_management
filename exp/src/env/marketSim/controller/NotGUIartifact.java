/**
 * 
 */
package marketSim.controller;

import java.io.File;
import java.util.Date;
import cartago.Artifact;
import cartago.ArtifactId;
import cartago.INTERNAL_OPERATION;
import cartago.LINK;
import cartago.OperationException;

/**
 * @author Nicolas
 *
 */
public class NotGUIartifact extends Artifact {

	private File selectedDataFile=null;
	protected Date date;


	/**
	 * 
	 */
	public NotGUIartifact(){
		selectedDataFile= new File("./data/scenario.xml");
	}
	
	@INTERNAL_OPERATION
	public void openFile()
	{
		ArtifactId simId=null;
	    try 
	   	{
			simId=lookupArtifact("marketPlace");
		} 
	   	catch (OperationException e) 
	   	{
			e.printStackTrace();
		}
	   	if(simId!=null)
	   	{
	   		try 
	   		{
				execLinkedOp(simId, "setFile", selectedDataFile);
			} 
	    	catch (OperationException e) 
	   		{
				e.printStackTrace();
			}
	    }
	}
	
	@INTERNAL_OPERATION
	public void run()
	{
		ArtifactId simId=null;
    	// run the marketPlace
		try 
    	{
			simId=lookupArtifact("marketPlace");
		} 
    	catch (OperationException e) 
    	{
			e.printStackTrace();
		}
    	if(simId!=null)
    	{
    		try 
    		{
				execLinkedOp(simId, "setRunning");
			} 
    		catch (OperationException e) 
    		{
				e.printStackTrace();
			}
    	}
    	// run the clock
    	try {
			execLinkedOp(lookupArtifact("clock"),"start");
		} catch (OperationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@LINK
	public void signalEndOfLoading()
	{
		run();
	}
	
	@LINK
	public void askForLoad()
	{
		openFile();
	}
	
	/**
	 * Print a text into the console
	 * @param text
	 */
	@LINK
	void print(String text)
	{
		System.out.print(text);
	}
	
	/**
	 * Add an agent to the tree 
	 * @param artifact
	 */
	@LINK
	public void addAgent(String agentName)
	{
		
	}
	
	/**
	 * Add an asset to the tree 
	 * @param artifact
	 */
	@LINK
	public void addAsset(String assetName)
	{

	}
	
	/**
	 * Add a quoted asset on a market
	 * @param 
	 */
	@LINK
	public void addQuotedAsset(String assetName, String venueName)
	{
		
	}
	
	/**
	 * Add a Portfolio to its owner in the tree
	 */
	public void addPortfolio()
	{
		// TODO implements this method to put a Portfolio in the tree
	}
	
	/**
	 * Add a venue in the "Venue" category of the tree
	 */
	@LINK
	public void addVenue(String venueName)
	{
		
	}
	
	/**
	 * Linked operation called by the clock artifact to update the date
	 * @param newDate the new date
	 */
	@LINK
	void updateDate(Date newDate)
	{
		date = newDate;
	}
}