/**
 * 
 */
package marketSim.controller;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.TreeMap;
import java.util.logging.Logger;

import marketSim.model.AbstractAsset;
import marketSim.model.AbstractTradingVenue;
import marketSim.model.Broker;
import marketSim.model.MarketSimModel;
import marketSim.model.Order;
import marketSim.model.Portfolio;
import marketSim.model.Side;
import cartago.Artifact;
import cartago.ArtifactId;
import cartago.GUARD;
import cartago.INTERNAL_OPERATION;
import cartago.LINK;
import cartago.OPERATION;
import cartago.OperationException;

/**
 * Artifact to communicate with agents
 * @author Nicolas Cointe
 * @version 0.1a
 */

public class MarketSimArtifact extends Artifact
{		
	protected LinkedList<ArtifactId> agentLinks = new LinkedList<ArtifactId>();
	protected LinkedList<String> agentNames = new LinkedList<String>();
	protected LinkedList<ArtifactId> humanTradersGUI = new LinkedList<ArtifactId>();
	protected LinkedList<String> humanNames = new LinkedList<String>();

	protected File dataFile= null;
	protected MarketSimModel model=null;
		
	boolean mutex = true;
	boolean loaded = false;
	boolean loadingEnded = false;

    
    // Logger
    protected static Logger logger = Logger.getLogger(MarketSimArtifact.class.getName());
	
	@OPERATION
    void init()
	{
    }
	
	@LINK
    void connect(ArtifactId id, String agentName) 
	{
		if(!loaded)
		{
			loaded=true;
			askForLoad();
		}
		if(! agentLinks.contains(id))
		{
			agentLinks.push(id);
			agentNames.push(agentName);
			updatePortfolioOfAgent(agentName);
		}
		addAgent(agentName);
	}
	
	@INTERNAL_OPERATION
	private void askForLoad() {
		ArtifactId guiId=null;
		try 
		{
			guiId=lookupArtifact("gui");
		}
		catch (OperationException e)
		{
			e.printStackTrace();
		}
		if(guiId==null)
		{
			System.out.print("Gui artifact not found");
			return;
		}
		try 
		{
			execLinkedOp(guiId, "askForLoad");
		} 
		catch (OperationException e)
		{
			e.printStackTrace();
		}

	}

	/**
	 * Linked operation used by an other artifact to be added in the diffusion list of a quoted asset
	 * @param id
	 * @param assetName
	 * @param venueName
	 */
	@LINK
	void subscribe(ArtifactId id, String assetName, String venueName)
	{
		model.addAssetSubscriber(id, venueName, assetName);
	}

	/**
	 * Operation used by the model to update the beliefs of agents
	 * @param id the id of the AgentLinkArtifact
	 * @param belief the belief to update
	 * @param value the new value to assign to the belief
	 */
	@OPERATION
	public void updateProperty(ArtifactId id, String belief, Object value)
	{
		try {
			execLinkedOp(id,"updateBelief", belief, value);
		} catch (OperationException e) {
			e.printStackTrace();
		}
	}

	@LINK
	public void setRunning() 
	{
		for(int i=0;i<agentLinks.size(); i++)
			updateProperty(agentLinks.get(i),"running", true);
		print("The simulation is now running.\n");
		
		for(int i=0;i<humanTradersGUI.size(); i++)
		{
			try {
				execLinkedOp(humanTradersGUI.get(i), "setRunning");

			} catch (OperationException e1) {
				e1.printStackTrace();
			}
		}
	}
	
	@LINK
	public void setFile(File file)
	{
		model = new MarketSimModel(this);
		model.setDate(Calendar.getInstance().getTime());
		try {
			execLinkedOp(lookupArtifact("clock"), "subscribe", getId());

		} catch (OperationException e1) {
			e1.printStackTrace();
		}
		model.deserialize(file);
		print(model.toString());
		
		// Updates the portfolios of the agents
		for(int i=0; i < agentLinks.size(); i++)
		{
			String agentName=agentNames.get(i);
			ArrayList<Portfolio> pf=model.getPortfolioOfAgent(agentName);
			try
			{
				execLinkedOp(agentLinks.get(i),"updatePorfolios",pf);
			} 
			catch (OperationException e) 
			{
				e.printStackTrace();
			}
		}
		signalEndOfLoading();
	}

	@LINK
	public void sell(ArtifactId id, String asset, int amount, String pfName, String options) throws InterruptedException
	{
		await("testMutex");
		mutex=false;
		//print("I want to sell " + amount + " " + model.getAssetByName(asset).getName() + " from my portfolio " + pfName + "\n");
		//print("Searching " + id + " in " + agentLinks + "\n");
		String agentName = agentNames.get(agentLinks.indexOf(id)); // TODO remove this bug
		ArrayList<Portfolio> pf=model.getPortfolioOfAgent(agentName);
		if(pf.size()==0) print("ERROR : they are no portfolio for agent " + agentName +"\n");
		for(int i=0; i<pf.size();i++)
		{
			//print("This one is " + pf.get(i).getName() + "\n");
			if(pf.get(i).getName().compareTo(pfName)==0)
			{
				//print("I'm looking for broker '"+ pf.get(i).getBroker() +"'\n");

				Broker broker = model.getBrokerByName(pf.get(i).getBroker());
				@SuppressWarnings("unused")
				boolean validity = broker.askSell(model.getAssetByName(asset),pf.get(i), amount, options);
				// TODO send a message to the agent if the request is not valid.
			}
		}
		print(model.toString());
		mutex=true;
	}
	
	@LINK
	public void buy(ArtifactId id, String asset, int amount, String pfName, String options) throws InterruptedException
	{
		await("testMutex");
		mutex=false;
		String agentName = agentNames.get(agentLinks.indexOf(id)); // TODO remove this bug
		ArrayList<Portfolio> pf=model.getPortfolioOfAgent(agentName);
		if(pf.size()==0) print("ERROR : they are no portfolio for agent " + agentName +"\n");
		for(int i=0; i<pf.size();i++)
		{
			if(pf.get(i).getName().compareTo(pfName)==0)
			{
				Broker broker = model.getBrokerByName(pf.get(i).getBroker());
				@SuppressWarnings("unused")
				boolean validity = broker.askBuy(model.getAssetByName(asset),pf.get(i), amount, options);
				// TODO send a message to the agent if the request is not valid.
			}
		}
		print(model.toString());
		mutex=true;
	}
	
	// execLinkedOp("out-1","cancelOrder", getId(), date, pfName, venue, side, asset, amount, (int) (price*1000.0));
	@LINK
	public void cancelOrder(ArtifactId id, Date date, String pfName, String venueName, Side side, String asset, int amount, int price) throws InterruptedException
	{
		await("testMutex");
		mutex=false;
		String agentName = agentNames.get(agentLinks.indexOf(id));
		AbstractTradingVenue venue = model.getVenueByName(venueName);
		ArrayList<Portfolio> pf=model.getPortfolioOfAgent(agentName);
		if(pf.size()==0) print("ERROR : they are no portfolio for agent " + agentName +"\n");
		for(int i=0; i<pf.size();i++)
		{
			if(pf.get(i).getName().compareTo(pfName)==0)
			{
				Broker broker = model.getBrokerByName(pf.get(i).getBroker());
				boolean validity = broker.cancel(model.getAssetByName(asset),pf.get(i), date, side, amount, price, venue);
				
				// Problem : sometime, the float is converted with an error.
				// We try to search the order with an heuristic approach by the nearest values
				// TODO implement a real solution
				for(int j=0; j<=10 && !validity; j++)
				{
					if(!validity) validity = broker.cancel(model.getAssetByName(asset),pf.get(i), date, side, amount, price+j, venue);
					if(!validity) validity = broker.cancel(model.getAssetByName(asset),pf.get(i), date, side, amount, price-j, venue);
				}
			}
		}
		print(model.toString());
		mutex=true;
	}
	
	@LINK
	public void print(String msg)
	{
		ArtifactId guiId=null;
		try 
		{
			guiId=lookupArtifact("gui");
		}
		catch (OperationException e)
		{
			e.printStackTrace();
		}
		if(guiId==null)
		{
			System.out.print(msg);
			return;
		}
		try 
		{
			execLinkedOp(guiId, "print", msg);
		} 
		catch (OperationException e)
		{
			//e.printStackTrace(); // always called. TODO : discover why.
		}
		
		for(int i=0;i<humanTradersGUI.size(); i++)
		{
			try {
				execLinkedOp(humanTradersGUI.get(i), "print", msg);

			} catch (OperationException e1) {
				e1.printStackTrace();
			}
		}
	}
	
	private ArtifactId getGUI()
	{
		ArtifactId guiId=null;
		try 
		{
			guiId=lookupArtifact("gui");
		}
		catch (OperationException e)
		{
			e.printStackTrace();
		}
		return guiId;
	}

	public File getSelectedDataFile()
	{
		return this.dataFile;
	}

	@LINK
	public void subscribeHumanTraderGUI(ArtifactId guiId, String name)
	{
		if(!humanTradersGUI.contains(guiId) && !humanNames.contains(name))
		{
			humanTradersGUI.add(guiId);
			humanNames.add(name);
		}
		else
		{
			print("ERROR : Name and/or id of this interface is yet used.");
			return;
		}
		
		if(loadingEnded==true) // Send the list of assets to the humanTraderGui
		{
			LinkedList<String> assetsList = new LinkedList<String>();
			for(int i=0; i< model.getAssets().size(); i++)
			{
				assetsList.addLast(model.getAssets().get(i).getName());
			}
			for(int i=0; i<humanTradersGUI.size(); i++)
				try {
					execLinkedOp(humanTradersGUI.get(i), "setAssetList", assetsList);
				} catch (OperationException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
		
		// subscribe for execution information of every quoted asset
		for(int i=0; i< model.getVenues().size(); i++) // for every marketplace
		{
			for(int j=0; j<model.getVenues().get(i).getQuotedAssets().size();j++) // for every asset on the marketplace
			{
				subscribe(guiId, 
						model.getVenues().get(i).getQuotedAssets().get(j).getAssetName(),
						model.getVenues().get(i).getName()
						);
			}
		}
	}
	
	@OPERATION
	public void addAsset(String nameString)
	{
		ArtifactId guiId=getGUI();
		if(guiId==null) return;
		try 
		{
			execLinkedOp(guiId, "addAsset", nameString);
		} 
		catch (OperationException e)
		{
			e.printStackTrace();
		}
		
		for(int i=0;i<humanTradersGUI.size(); i++)
		{
			try {
				execLinkedOp(humanTradersGUI.get(i), "addAsset", nameString);

			} catch (OperationException e1) {
				e1.printStackTrace();
			}
		}
	}

	@OPERATION
	public void addVenue(String nameString)
	{
		ArtifactId guiId=getGUI();
		if(guiId==null) return;
		try 
		{
			execLinkedOp(guiId, "addVenue", nameString);
		} 
		catch (OperationException e)
		{
			e.printStackTrace();
		}
	}
	
	@OPERATION
	public void addQuotedAsset(String assetName, String venueName)
	{
		ArtifactId guiId=getGUI();
		if(guiId==null) return;
		try 
		{
			execLinkedOp(guiId, "addQuotedAsset", assetName, venueName);
		} 
		catch (OperationException e)
		{
			e.printStackTrace();
		}
	}
	
	@OPERATION
	public void addAgent(String nameString)
	{
		ArtifactId guiId=getGUI();
		if(guiId==null) return;
		try 
		{
			execLinkedOp(guiId, "addAgent", nameString);
		} 
		catch (OperationException e)
		{
			e.printStackTrace();
		}
	}
	
	public void updatePortfolioOfAgent(String agentName)
	{
		ArtifactId id;
		if(agentNames.contains(agentName))
			id=agentLinks.get(agentNames.indexOf(agentName));
		else 
		{
			if(humanNames.contains(agentName))
				id=humanTradersGUI.get(humanNames.indexOf(agentName));
			else 
			{
				print("ERROR : agent or human called " + agentName + " not found.\n");
				return;
			}
		}
		
		if(model!=null)
		{
			ArrayList<Portfolio> pf=model.getPortfolioOfAgent(agentName);
			try {
				execLinkedOp(id,"updatePorfolios",pf);
			} 
			catch (OperationException e) {
				e.printStackTrace();
			}
		}
	}
	
	@GUARD
	boolean testMutex()
	{
		return mutex;
	}
	
	@OPERATION
	public void signalOrderAddition(ArtifactId artifactId, AbstractTradingVenue venue, Order order)
	{
		try {
			execLinkedOp(artifactId,"signalAdditionOrder", venue, order);
		} 
		catch (OperationException e) {
			e.printStackTrace();
		}
	}
	
	@OPERATION
	public void signalOrderGeneration(ArtifactId artifactId, AbstractTradingVenue venue, Order order, Date date)
	{
		
	}

	@OPERATION
	public void signalOrderExecution(ArtifactId artifactId, AbstractTradingVenue venue, Order newOrder, Order oldOrder, int quantity, Date date)
	{
		try {
			execLinkedOp(artifactId,"signalExecutionOrder", venue, newOrder, oldOrder, quantity, date);
		}
		catch (OperationException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Linked operation called by the clock artifact to update the date
	 * @param newDate the new date
	 */
	@LINK
	void updateDate(Date newDate)
	{
		model.setDate(newDate);
		model.onClockTick(newDate);
	}

	@OPERATION
	public void signalIndicatorChanges(ArtifactId artifactId, Date dateOfEnd,
			AbstractTradingVenue venue, AbstractAsset asset, float x_close,
			long v, float intensity, float average, float doubleLengthAverage,
			float bollingerUp, float bollingerDown) 
	{
		try {
			execLinkedOp(artifactId,"signalIndicatorChanges", dateOfEnd, venue, asset, x_close, v, intensity, average, doubleLengthAverage, bollingerUp, bollingerDown);
		}
		catch (OperationException e) {
			e.printStackTrace();
		}
	}

	@OPERATION
	public void signalOrderCancelation(ArtifactId artifactId, AbstractTradingVenue venue, Order order) 
	{
		try {
			execLinkedOp(artifactId,"signalCancelationOrder", venue, order);
		}
		catch (OperationException e) {
			e.printStackTrace();
		}
	}
	
	@INTERNAL_OPERATION
	public void signalEndOfLoading()
	{
		ArtifactId guiId=null;
		try 
		{
			guiId=lookupArtifact("gui");
		}
		catch (OperationException e)
		{
			e.printStackTrace();
		}
		if(guiId==null)
		{
			// do something
			return;
		}
		try 
		{
			execLinkedOp(guiId, "signalEndOfLoading");
		} 
		catch (OperationException e)
		{
			//e.printStackTrace(); // always called. TODO : discover why.
		}
		
		// Send the list of assets to all the humanGUI
		LinkedList<String> assetsList = new LinkedList<String>();
		for(int i=0; i< model.getAssets().size(); i++)
			assetsList.addLast(model.getAssets().get(i).getName());
		
		loadingEnded=true;
		
		for(int i=0; i<humanTradersGUI.size(); i++)
			try {
				execLinkedOp(humanTradersGUI.get(i), "setAssetList", assetsList);
			} catch (OperationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		
	}

	@INTERNAL_OPERATION
	public void sendDuration(int duration) {
		ArtifactId clockId=null;
		try 
		{
			clockId=lookupArtifact("clock");
		}
		catch (OperationException e)
		{
			e.printStackTrace();
		}
		if(clockId==null)
		{
			// do something
			return;
		}
		try 
		{
			execLinkedOp(clockId, "setDuration", duration);
		} 
		catch (OperationException e)
		{
			e.printStackTrace();
		}
	}
	
	@LINK
	public void createPortfolio(String name, String holder, String broker, Object [] assets )
	{
		AbstractAsset asset = null;

		if( ! model.createPortfolio(name, holder, broker))
		{
			print("ERROR : This porfolio already exists !");
			return;
		}
		
		Portfolio pf=model.getPortfolioByName(name);
		for(int i=0; i<assets.length; i++)
		{
			if(assets[i].getClass()==String.class) asset=model.getAssetByName( (String) assets[i]);
			else
			{
				if(assets[i].getClass()==Integer.class) model.addInPortfolio(asset, (Integer) assets[i], pf);
				else print("error with argument number " +i);
			}
		}
		updatePortfolioOfAgent(holder); // update the info
	}

	public void askForLogImages() 
	{
		for(int i=0;i<agentLinks.size(); i++)
		{
			try {
				execLinkedOp(agentLinks.get(i), "updateImagesLog");
			} catch (OperationException e) {
				e.printStackTrace();
			}
		}
	}
}
