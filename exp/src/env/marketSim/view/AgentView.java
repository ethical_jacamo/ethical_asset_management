/**
 * 
 */
package marketSim.view;

import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SpringLayout;

import net.infonode.docking.View;
/**
 * 
 * @author Nicolas
 *
 */
public class AgentView extends View {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9046850804327582989L;

	private String agent=null;
	private JComponent component=null;
	
	// Graphical elements
	private JLabel nameLabel = null;
	private JTextField nameTextField = null;
	private JButton webInterfaceButton=null;
	
	/**
	 * Constructor inherited from net.infonode.docking.View
	 * @param title the title of the view pane
	 * @param icon the icon of the pane
	 * @param component the component to put into the view
	 */
	public AgentView(Icon icon, JComponent compo, final String agent)
	{
		super(agent, icon, compo);
		this.nameTextField=new JTextField(agent,20);
		this.nameTextField.setEditable(false);
		this.nameLabel=new JLabel("Name of the agent :");
		this.webInterfaceButton=new JButton("see web interface");
		this.webInterfaceButton.setSize(20,this.webInterfaceButton.getHeight());
		this.component=compo;
		this.agent=agent;
		
		// Creation of the layout
        SpringLayout layout = new SpringLayout();
		this.component.setLayout(layout);
		this.component.add(this.nameLabel);
		this.nameLabel.setLabelFor(this.nameTextField);
		this.component.add(nameTextField);
		
        this.component.add(this.webInterfaceButton);

        layout.putConstraint(SpringLayout.WEST, this.nameTextField, 5, SpringLayout.EAST, this.nameLabel);
        layout.putConstraint(SpringLayout.NORTH, this.nameLabel, 5, SpringLayout.NORTH, this.component);
        layout.putConstraint(SpringLayout.NORTH, this.webInterfaceButton, 5, SpringLayout.SOUTH, this.nameTextField);
        
        this.webInterfaceButton.addActionListener(new ActionListener() 
        	{
            	public void actionPerformed(ActionEvent e)
            	{
            		try 
            		{
            			openWebpage(new URL("http://127.0.0.1:3272/agent-mind/" + agent + "/latest"));
            		}
            		catch (MalformedURLException e1) 
            		{
            			// TODO Auto-generated catch block
            			e1.printStackTrace();
            		}
            	}
        	}
        );
        
	}
	
	public String getAgentName()
	{
		return this.agent;
	}
	
	public static void openWebpage(URI uri) {
	    Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
	    if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
	        try {
	            desktop.browse(uri);
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	    }
	}

	public static void openWebpage(URL url) {
	    try {
	        openWebpage(url.toURI());
	    } catch (URISyntaxException e) {
	        e.printStackTrace();
	    }
	}
}
