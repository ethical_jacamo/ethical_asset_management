/**
 * 
 */
package marketSim.view;

import java.util.Observable;
import java.util.Observer;

import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeSelectionModel;

/**
 * @author Nicolas
 *
 */
class SimJTree extends JTree implements Observer{

	/**
	 * Generated serial number
	 */
	private static final long serialVersionUID = -5696375226152222897L;
	private DefaultMutableTreeNode root;

	public SimJTree()
	{
		super(new DefaultTreeModel(new DefaultMutableTreeNode("Simulation")));
		DefaultTreeModel model = (DefaultTreeModel) this.getModel();
		//DefaultMutableTreeNode this.root = (DefaultMutableTreeNode) model.getRoot();
		this.root = new DefaultMutableTreeNode("Simulator");
		DefaultMutableTreeNode nodeAgents=new DefaultMutableTreeNode("Agents");
		model.insertNodeInto(nodeAgents, this.root, this.root.getChildCount());
		this.setRootVisible(true);
		this.setShowsRootHandles(true);
		buildTree();
		treeDidChange();
		repaint();
	}
	
	protected void buildTree()
	{
		DefaultTreeModel treeModel = (DefaultTreeModel) this.getModel();
		this.setEditable(false);
		this.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
		
		DefaultMutableTreeNode category= null;
		// agents
		category = new DefaultMutableTreeNode("Agents");
		treeModel.insertNodeInto(category, this.root, this.root.getChildCount());
		// venues
		category = new DefaultMutableTreeNode("Venues");
		treeModel.insertNodeInto(category, this.root, this.root.getChildCount());
		treeModel.reload(root);
	}
	
	protected void addAgent(String agentName)
	{
		
	}
	
	public void clear()
	{
		this.root.removeAllChildren();
		this.setModel(null);
		//setRootVisible(false);
	}

	public void update(Observable o, Object arg)
	{
				
	}
}
