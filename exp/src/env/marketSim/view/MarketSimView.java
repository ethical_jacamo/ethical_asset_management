/**
 * 
 */
package marketSim.view;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.Date;
import java.util.Enumeration;
// TODO Implement serialization and deserialization of the ui
/*import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;*/
import java.util.LinkedList;

import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTree;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.text.DefaultCaret;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;

import cartago.ArtifactId;
import cartago.INTERNAL_OPERATION;
import cartago.LINK;
import cartago.OperationException;
import cartago.tools.GUIArtifact;
import net.infonode.docking.*;
import net.infonode.docking.properties.RootWindowProperties;
import net.infonode.docking.util.DockingUtil;
import net.infonode.docking.util.PropertiesUtil;
import net.infonode.docking.util.ViewMap;

/**
 * This view mix Cartago, infonode and swing components to display a user interface with dockable windows.
 * @author Nicolas
 */
public class MarketSimView extends GUIArtifact
{
	private static final String SIMTREE = "simtree";
	private static final String CONSOLE = "console";
	
	private ViewMap viewMap;
	private LinkedList<View> temporaryViews;
	private JTextArea console;
	private RootWindow rootWindow;
	private JTree simTree;
	protected JFrame jFrame;
	protected Date date;
	protected JLabel dateLabel;

	JFileChooser fc=null;
	private File selectedDataFile=null;
	
	protected LinkedList<ArtifactId> simList = new LinkedList<ArtifactId>();
	
	// Swing components
	JMenu menuSimulator,newSubmenu;
	JMenuItem openButton;
	JMenuItem runButton;
	JMenuItem quitButton;
	JMenuItem TradingWindowButton;
	
	public void setup()
	{
		this.jFrame= new JFrame();
		this.console = new JTextArea();
		DefaultCaret caret = (DefaultCaret)this.console.getCaret();
		caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
		this.temporaryViews= new LinkedList<View>();
		build();
	    this.jFrame.setVisible(true);
	}
	
	/**
	 * Build the main window
	 */
	private void build()
	{
		this.jFrame.setTitle("Ethical market simulator");	
		this.jFrame.setSize(1200,900);
		this.jFrame.setLocationRelativeTo(null);
		this.jFrame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		
		JMenuBar menuBar = new JMenuBar();
		
		// Menu "Simulator"
		menuSimulator = new JMenu("Simulation");
		runButton = new JMenuItem("Run");
		runButton.setEnabled(false);
		menuSimulator.add(runButton);
		openButton = new JMenuItem("Open");
		menuSimulator.add(openButton);
		menuSimulator.addSeparator();
		newSubmenu = new JMenu("New");
			TradingWindowButton = new JMenuItem("Trading Interface");
			TradingWindowButton.setEnabled(false);
			newSubmenu.add(TradingWindowButton);
		menuSimulator.add(newSubmenu);
		
		menuSimulator.addSeparator();
		quitButton = new JMenuItem("Quit");
		menuSimulator.add(quitButton);
		
		menuBar.add(menuSimulator);
		dateLabel = new JLabel();
		date = new Date();
		dateLabel.setText(date.toString());
        menuBar.add(Box.createHorizontalGlue());
		menuBar.add(dateLabel);
		
		this.jFrame.setJMenuBar(menuBar);
		
		this.viewMap=new ViewMap();
		View simView= new View("Project", new ImageIcon("./img/projectIcon.png"), createViewComponent(SIMTREE));
		this.viewMap.addView(0, simView);
		//View tabPaneView= new View("", new ImageIcon("./img/scopeIcon.png"), createViewComponent(TABPANE));
		//this.viewMap.addView(1, tabPaneView);
		View consoleView= new View("Console", new ImageIcon("./img/consoleIcon.png"), createViewComponent(CONSOLE));
		this.viewMap.addView(2, consoleView);
		
		this.rootWindow=DockingUtil.createRootWindow(this.viewMap,true);
		
		// Enable title bar style
		RootWindowProperties titleBarStyleProperties = PropertiesUtil.createTitleBarStyleRootWindowProperties();
		rootWindow.getRootWindowProperties().addSuperObject(titleBarStyleProperties);
		
		// Add a listener on the tree
		linkMouseEventToOp(this.simTree, "mousePressed", "clickOnTree");		//bug generator
		jFrame.setContentPane(rootWindow);

	    linkActionEventToOp(this.openButton,"open");
	    linkActionEventToOp(this.runButton,"run");
	    linkActionEventToOp(this.quitButton,"closed");
	    linkActionEventToOp(this.TradingWindowButton,"createTradingWindow");
	}

	/**
	 * Create a component of the main window
	 * @param text name of the component's type
	 * @return the created component
	 */
	private Component createViewComponent(String text)
	{
		if(text.equals(SIMTREE)) 
		{
			this.simTree= null;
			DefaultMutableTreeNode top = new DefaultMutableTreeNode("Simulation");
			top.add(new DefaultMutableTreeNode("Agents"));
			top.add(new DefaultMutableTreeNode("Venues"));
			top.add(new DefaultMutableTreeNode("Assets"));
			this.simTree = new JTree(top);
			return new JScrollPane(this.simTree);
		}
		if(text.equals(CONSOLE)) return new JScrollPane(this.console);
		
		return new JScrollPane(new JTextArea());
	}
	
	/**
	 * Print a text into the console of the main window
	 * @param text
	 */
	@LINK
	void print(String text)
	{
		try
		{
			this.console.append(text);
		}
		catch(Exception e)
		{
			System.out.println("ERROR : unable to print '" + text + "' in console.");
		}
	}
	
	@LINK
	void askForLoad()
	{
		
	}
	
	@LINK
	void subscribeSim(ArtifactId id)
	{
		if(!simList.contains(id))
		{
			this.simList.push(id);
			execInternalOp("print", "Artifact Id added\n");
		}
	}
	
	@LINK
	void signalEndOfLoading()
	{
		print("scenario loaded\n");
	}
	
	/**
	 * Add an agent to the tree 
	 * @param artifact
	 */
	@LINK
	public void addAgent(String agentName)
	{
		DefaultMutableTreeNode root = (DefaultMutableTreeNode) this.simTree.getModel().getRoot();
		((DefaultMutableTreeNode) root.getChildAt(0)).add(new DefaultMutableTreeNode(agentName));
		((DefaultTreeModel) this.simTree.getModel()).reload(root);
	}
	
	/**
	 * Add an asset to the tree 
	 * @param assetName The name of the asset added
	 */
	@LINK
	public void addAsset(String assetName)
	{
		// TODO Verify that the asset is not yet in the tree.
		DefaultMutableTreeNode root = (DefaultMutableTreeNode) this.simTree.getModel().getRoot();
		((DefaultMutableTreeNode) root.getChildAt(2)).add(new DefaultMutableTreeNode(assetName));
		((DefaultTreeModel) this.simTree.getModel()).reload(root);
	}
	
	/**
	 * Add a quoted asset on a market
	 * @param 
	 */
	@LINK
	public void addQuotedAsset(String assetName, String venueName)
	{
		// TODO Verify that the asset is not yet in the tree.
		DefaultMutableTreeNode root = (DefaultMutableTreeNode) this.simTree.getModel().getRoot();
		@SuppressWarnings("unchecked")
		Enumeration<TreeNode> childList = ((DefaultMutableTreeNode) root.getChildAt(1)).children();
		while(childList.hasMoreElements())
		{
			DefaultMutableTreeNode child=(DefaultMutableTreeNode) childList.nextElement();
			if(child.toString().compareTo(venueName)==0)
				child.add(new DefaultMutableTreeNode(assetName));
		}
		((DefaultTreeModel) this.simTree.getModel()).reload(root);
	}
	
	/**
	 * Add a Portfolio to its owner in the tree
	 */
	public void addPortfolio()
	{
		// TODO implements this method to put a Portfolio in the tree
	}
	
	/**
	 * Add a venue in the "Venue" category of the tree
	 */
	@LINK
	public void addVenue(String venueName)
	{
		DefaultMutableTreeNode root = (DefaultMutableTreeNode) this.simTree.getModel().getRoot();
		((DefaultMutableTreeNode) root.getChildAt(1)).add(new DefaultMutableTreeNode(venueName));
		((DefaultTreeModel) this.simTree.getModel()).reload(root);
	}
	
	/**
	 * Method called by the model to obtain the xml scenario file
	 * @return this.selectedDataFile typically to deserialize the model.
	 */
	public File getSelectedDataFile()
	{
		return this.selectedDataFile;
	}
	
	public void addAgentView(String agent)
	{
		for(int i=0; i<this.temporaryViews.size();i++)
		{
			if(AgentView.class.isInstance( this.temporaryViews.get(i)))
			{
				if(((AgentView) this.temporaryViews.get(i)).getAgentName()==agent)
				{
					this.temporaryViews.get(i).requestFocus(); // Don't works yet :'(
					return;
				}
			}
		}
		ImageIcon icon =new ImageIcon("./img/agent.png");
		AgentView av=new AgentView(icon,new JPanel(),agent);
		this.temporaryViews.add(av);
		DockingUtil.addWindow(av, this.rootWindow);
	}
	
	public void addAssetView(String assetName, String marketName)
	{
		for(int i=0; i<this.temporaryViews.size();i++)
		{
			if(AssetView.class.isInstance( this.temporaryViews.get(i)))
			{
				if(((AssetView) this.temporaryViews.get(i)).getAssetName()==assetName
						&& ((AssetView) this.temporaryViews.get(i)).getMarketName()==assetName )
				{
					this.temporaryViews.get(i).requestFocus(); // Don't works yet :'(
					return;
				}
			}
		}
		ImageIcon icon =new ImageIcon("./img/asset.png");
		AssetView av=new AssetView(icon,new JPanel(),assetName, marketName);
		this.temporaryViews.add(av);
		DockingUtil.addWindow(av, this.rootWindow);
	}

	@INTERNAL_OPERATION
	void closed(ActionEvent ev)
	{
		int confirm = JOptionPane.showOptionDialog(
	             null, "Are You Sure to Close Application?", 
	             "Exit Confirmation", JOptionPane.YES_NO_OPTION, 
	             JOptionPane.QUESTION_MESSAGE, null, null, null);
	    if (confirm == 0) 
	    {
	    	signal("closed");
	    	System.exit(0);
	    }
	}
	
	@INTERNAL_OPERATION
	void open(ActionEvent ev)
	{
		try {
			execLinkedOp(lookupArtifact("clock"), "subscribe", getId());

		} catch (OperationException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		launchFileChooser();
		while(selectedDataFile == null)
		{
			try {
				Thread.sleep(500);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		ArtifactId simId=null;
	    try 
	   	{
			simId=lookupArtifact("marketPlace");
		} 
	   	catch (OperationException e) 
	   	{
			e.printStackTrace();
		}
	   	if(simId!=null)
	   	{
	   		this.simList.add(simId);
	   		try 
	   		{
				execLinkedOp(simId, "setFile", selectedDataFile);
			} 
	    	catch (OperationException e) 
	   		{
				e.printStackTrace();
			}
	   		print("Fini");
	   		this.runButton.setEnabled(true);
	    	this.openButton.setEnabled(false);
    		this.TradingWindowButton.setEnabled(true);

	    }
	}
	
	@INTERNAL_OPERATION
	void clickOnTree(MouseEvent e)
	{
		//print("'clickOnTree' internal operation called\n");
		int selRow = simTree.getRowForLocation(e.getX(), e.getY());
		TreePath selPath = simTree.getPathForLocation(e.getX(), e.getY());
		if(selRow != -1) 
		{
			if(e.getClickCount() == 2) 
			{
				DefaultMutableTreeNode selectedNode = (DefaultMutableTreeNode) selPath.getLastPathComponent();
				//print("doubleclic on " + selectedNode.getUserObject() + "\n");
				if(selectedNode.getParent().toString().equals("Agents"))
				{
					addAgentView((String) selectedNode.getUserObject());
				}
				if(selectedNode.getParent().getParent().toString().equals("Venues"))
				{
					addAssetView((String) selectedNode.getUserObject(), selectedNode.getParent().toString());
				}
			}
		}
	}
	
	@INTERNAL_OPERATION
	void run(ActionEvent ev)
	{
    	ArtifactId simId=null;
    	// run the marketPlace
		try 
    	{
			simId=lookupArtifact("marketPlace");
			
		} 
    	catch (OperationException e) 
    	{
			e.printStackTrace();
		}
    	if(simId!=null)
    	{
    		this.simList.add(simId);
    		try 
    		{
				execLinkedOp(simId, "setRunning");
			} 
    		catch (OperationException e) 
    		{
				e.printStackTrace();
			}
    		this.runButton.setEnabled(false);
    	}
    	// run the clock
    	try {
			execLinkedOp(lookupArtifact("clock"),"start");
		} catch (OperationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@INTERNAL_OPERATION
	void createTradingWindow(ActionEvent ev)
	{
		signal("createTradingInterface");
	}
	
	/**
	 * Create a separated thread to avoid any scarcity problem
	 */
	public void launchFileChooser()
	{
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                try {
                    UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
                } catch(Exception e) {
                    e.printStackTrace();
                }
                fc=new JFileChooser();
        		fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
                fc.setFileFilter(new FileNameExtensionFilter("XML files (*.xml)", "xml"));
        		fc.setDialogTitle("Please select an experience configuration file");
        		fc.setCurrentDirectory(new File("./data/"));
                if (fc.showOpenDialog(null) == JFileChooser.APPROVE_OPTION)
                {
                	selectedDataFile = fc.getSelectedFile();
                }
            }
        });
    }

	/**
	 * Linked operation called by the clock artifact to update the date
	 * @param newDate the new date
	 */
	@LINK
	void updateDate(Date newDate)
	{
		date = newDate;
		dateLabel.setText(date.toString());
	}
}