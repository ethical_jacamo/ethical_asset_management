/**
 * 
 */
package marketSim.view;

import java.awt.Graphics2D;

import javax.swing.Icon;
import javax.swing.JComponent;

import net.infonode.docking.View;

/**
 * @author Nicolas
 *
 */
public class AssetView extends View {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8912895359690735543L;
	
	protected String assetName = null;
	protected String marketName = null;
	
	protected Graphics2D graph = null; // not adapted

	/**
	 * @param icon Icon of the view
	 * @param compo Component inside the  
	 * @param arg2
	 */
	public AssetView(Icon icon, JComponent compo, final String assetName, final String marketName) {
		super(assetName + " - " + marketName, icon, compo);
		this.assetName=assetName;
		this.marketName=marketName;
	}

	public String getAssetName() {
		return assetName;
	}

	public String getMarketName() {
		return marketName;
	}

}
