/**
 * 
 */
package marketSim.view;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;

/**
 * @author Nicolas
 *
 */
public class ConfigDialog extends JInternalFrame implements ActionListener
{

	/**
	 * Generated serial ID
	 */
	private static final long serialVersionUID = 6956751188643434673L;
	
	private JLabel labelTypeSimulation;
	private JComboBox<String> typeSimulation;

	public ConfigDialog()
	{
		super();
		this.setLayout(new GridLayout(1, 2, 15, 15));
		this.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
		
		labelTypeSimulation = new JLabel("Type of Scenario : ");
		String[] typeSimulationData = {"Simulation and Experimentation", "Simulation", "Experimentation"};
		typeSimulation = new JComboBox<String>(typeSimulationData);
		typeSimulation.addActionListener(this);
		
		this.getContentPane().add(labelTypeSimulation);
		this.getContentPane().add(typeSimulation);
		
		this.setVisible(true);
	}

	public void actionPerformed(ActionEvent e)
	{
		// TODO Auto-generated method stub
		
	}
	
}
