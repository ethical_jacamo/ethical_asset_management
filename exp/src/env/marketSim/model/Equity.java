package marketSim.model;

public class Equity extends AbstractSecurity {
	protected String sector;
	protected String subsector;
	
	public Equity(String name, String symbol, String isin, String sector, String subsector)
	{
		super(name, symbol, isin);
		this.sector=sector;
		this.subsector=subsector;
	}

	
	public Equity(String name, String symbol, String isin, String sector, String subsector, String comments)
	{
		super(name, symbol, isin, comments);
		this.sector=sector;
		this.subsector=subsector;
	}
}
