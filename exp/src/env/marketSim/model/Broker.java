package marketSim.model;

import java.util.Date;
import java.util.LinkedList;

public class Broker
{
	protected float pips;
	protected Currency pipsCurrency;
	protected String name;
	protected LinkedList<AbstractTradingVenue> markets = new LinkedList<AbstractTradingVenue>();
	protected MarketSimModel model;
	

	public Broker(MarketSimModel model, String name, float pips, Currency pipsCurrency)
	{
		this.pips=pips;
		this.pipsCurrency=pipsCurrency;
		this.name=name;
		this.model=model;
	}
	
	protected void fillOrder(Order order, int volume, Currency currency, float price)
	{
		//model.artifact.print("Fill " + order.getAsset().getName() + " v=" + volume + " for " + price + " " + currency.getSymbol() + "\n");
		Portfolio portfolio = order.getPortfolio();
		if (order.getSide() == Side.ASK) // sell order
		{
			portfolio.addAssets(currency, volume * ((int) (price*1000)));
			portfolio.addAssets(order.getAsset(), 0-volume);
		}
		else // Buy order
		{
			int cost = 0-(volume *((int)(price*1000.0)));
			portfolio.addAssets(currency, cost);
			portfolio.addAssets(order.getAsset(), volume);
		}
		order.setQuantity(order.getQuantity()-volume);
		model.artifact.print(portfolio.toString());
		model.artifact.updatePortfolioOfAgent(order.getPortfolio().getHolder());
	}
	
	protected void addMarket(AbstractTradingVenue market)
	{
		for(int i=0; i<this.markets.size();i++)
			if(this.markets.get(i).getMic().equals(market.getMic())) // if the market is already in the list
				return;
		this.markets.add(market);
	}

	public String getName() {
		return this.name;
	}

	public boolean askSell(AbstractAsset asset, Portfolio portfolio, int amount, String options) 
	{
		System.out.println("askSell ! asset : " + asset.getName() + " portfolio : " + portfolio.getName() + " amount : " + amount + " options : " + options);
		System.out.println("value for asset : " + portfolio.getVallueOfAssetByName(asset.getName()));
		System.out.println(portfolio.toString());
		
		if(portfolio.getVallueOfAssetByName(asset.getName())>=amount)
		{
			for(int i=0; i<markets.size(); i++)
			{
				if(markets.get(i).isQuoted(asset))
				{
					Order order = new Order(asset, Side.ASK, amount, this, portfolio);
					parseOptions(order, options);
					return markets.get(i).placeOrder(order);
				}
			}
		}
		return false;
	}

	public boolean askBuy(AbstractAsset asset, Portfolio portfolio, int amount, String options)
	{
		// System.out.println("askBuy ! asset : " + asset.getName() + " portfolio : " + portfolio.getName() + " amount : " + amount + " options : " + options);
		// System.out.println("value for asset : " + portfolio.getVallueOfAssetByName(asset.getName()));
		// System.out.println(portfolio.toString());
		for(int i=0; i<markets.size(); i++)
		{
			if(markets.get(i).isQuoted(asset))
			{
				if(portfolio.getVallueOfAssetByName(markets.get(i).currency.getName())<1000) // TODO implement a real verification
				{
					System.out.println("ERROR : Order rejected by broker");
					return false;
				}
				Order order = new Order(asset, Side.BID, amount, this, portfolio);
				parseOptions(order, options);
				return markets.get(i).placeOrder(order);
			}
		}
		return false;
	}

	public boolean cancel(AbstractAsset asset, Portfolio portfolio, Date date, Side side, int amount, int price, AbstractTradingVenue venue)
	{
		return(venue.removeOrder(date,side, asset, portfolio, amount, price));
	}
	
	private void parseOptions(Order order, String options)
	{
		String[] opt=options.split(" ");
		if(options.compareTo("")==0) return;
		for(int i=0; i<opt.length; i++)
		{
			switch(opt[i])
			{
				case "LMT":
					order.setPriceType(PriceType.LMT);
					order.setLimit(((float)((int)(Float.parseFloat(opt[i+1]) * 1000)))/(float)1000.0);
					i++;
					break;
				case "MKT":
					order.setPriceType(PriceType.MKT);
					break;
				default :
					System.out.println("ERROR : unrecognised option '" + opt[i] + "'");
			}
		}
	}
}
