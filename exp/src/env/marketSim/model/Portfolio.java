package marketSim.model;

import java.util.ArrayList;

public class Portfolio
{
	protected String name;
	protected String holder;
	//protected HashMap<AbstractAsset, Integer> assets = new HashMap<AbstractAsset, Integer>();
	protected ArrayList<AbstractAsset> assets = new ArrayList<AbstractAsset>();
	protected ArrayList<Integer> values = new ArrayList<Integer>();
	protected String broker;
	
	/**
	 * Constructor of the Portfolio
	 * @param name the name of the portfolio
	 * @param holder the name of the agent who own this portfolio
	 */
	public Portfolio(String name, String holder, String broker)
	{
		this.name=name;
		this.holder=holder;
		this.broker=broker;
	}
	
	/*public HashMap<AbstractAsset, Integer> getAssets()
	{
		return this.assets;
	}*/
	
	public int getValueForAsset(AbstractAsset asset)
	{
		if(assets.contains(asset)) return values.get(assets.indexOf(asset));
		return 0;
	}
	
	public AbstractAsset getAnyAsset()
	{
		if (assets.size()>0)
			return assets.get(0);
		return null;
	}
	
	public void addAssets(AbstractAsset asset, int quantity)
	{
		int current=0;
		if(assets.contains(asset))
			current=getValueForAsset(asset);
		else 
		{
			assets.add(asset);
			values.add(0);
		}
		if(current+quantity!=0)
			values.set(assets.indexOf(asset), current+quantity);
		else
		{
			values.remove(assets.indexOf(asset));
			assets.remove(assets.indexOf(asset));
		}
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the broker
	 */
	public String getBroker() {
		return broker;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @param assets the assets to set
	 */
	/*public void setAssets(HashMap<AbstractAsset, Integer> assets) {
		this.assets = assets;
	}*/

	/**
	 * @param broker the broker to set
	 */
	public void setBroker(String broker) {
		this.broker = broker;
	}

	public String getHolder() {
		return holder;
	}
	
	public int size()
	{
		return assets.size();
	}

	public void remove(AbstractAsset asset) 
	{
		values.remove(assets.indexOf(asset));
		assets.remove(asset);
	}
	
	public int getVallueOfAssetByName(String assetName)
	{
		for(int i=0; i<assets.size(); i++)
		{
			if(assets.get(i).name.compareTo(assetName)==0)
				return values.get(i);
		}
		return 0;
	}
	
	public ArrayList<AbstractAsset> getAssets()
	{
		return assets;
	}
	
	public String toString()
	{
		String s="Portfolio " + name + " :\n";
		for(int i=0; i<assets.size();i++)
			s = s + "  " + values.get(i) + " " + assets.get(i).getName() + "\n";
		return s;
	}
	
	public AbstractAsset get(int index)
	{
		return assets.get(index);
	}
}
