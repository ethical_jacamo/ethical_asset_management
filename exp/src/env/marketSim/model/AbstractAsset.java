package marketSim.model;

public abstract class AbstractAsset
{
	protected String name;
	protected String symbol;
	protected String comments;
	
	protected AbstractAsset(String name, String symbol)
	{
		this(name,symbol,"");
	}
	
	protected AbstractAsset(String name, String symbol, String comments)
	{
		this.name=name;
		this.symbol=symbol;
		this.comments=comments;
	}

	public String getName() {
		return this.name;
	}
	
	public String getSymbol()
	{
		return this.symbol;
	}
}
