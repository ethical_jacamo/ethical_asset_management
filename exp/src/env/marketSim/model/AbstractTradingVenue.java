/**
 * 
 */
package marketSim.model;

import java.lang.reflect.InvocationTargetException;
import java.util.Date;
import java.util.LinkedList;

/**
 * @author Nicolas
 *
 */
public abstract class AbstractTradingVenue {

	/**
	 * 
	 */
	protected LinkedList<PriceType> acceptablePriceType = new LinkedList<PriceType>();
	protected LinkedList<OrderCondition> acceptableOrderCondition = new LinkedList<OrderCondition>();
	protected LinkedList<OrderDuration> acceptableOrderDuration = new LinkedList<OrderDuration>();
	protected AbstractMatchingMethod matchingMethod = null;
	protected LinkedList<AbstractMatchingMethod> quotedAssets=null;
	protected String name;
	protected String country;
	protected String city;
	protected String mic;
	protected Currency currency;
	protected String comments;
	protected MarketState state=MarketState.CLOSE;
	
	protected MarketSimModel model;
	
	public AbstractTradingVenue(MarketSimModel model, AbstractMatchingMethod matching, String name, String country, String city, String mic, Currency currency)
	{
		this(model, matching, name, country, city, mic, currency, "");
	}

	
	public AbstractTradingVenue(MarketSimModel model, AbstractMatchingMethod matching, String name, String country, String city, String mic, Currency currency, String comments)
	{
		this.model = model;
		this.matchingMethod=matching;
		this.name=name;
		this.country=country;
		this.city=city;
		this.mic=mic;
		this.currency=currency;
		this.comments=comments;
		this.quotedAssets=new LinkedList<AbstractMatchingMethod>();
	}

	public boolean placeOrder(Order order)
	{
		if(this.isAcceptable(order))
		{
			getQuotedAsset(order.getAsset().getName()).placeOrder(order);
			return true;
		}
		return false;
	}
	
	protected boolean isAcceptable(Order order)
	{	
		// test if this asset is quoted
		if(! isQuoted(order.getAsset()))
		{
			System.out.println("this asset is not quoted\n");
			return false;
		}
		
		// test if the price type is acceptable
		if(! this.acceptablePriceType.contains(order.getPriceType()))
		{
			System.out.println("this price type is not acceptable\n");
			return false;
		}
		if(order.getPriceType()==PriceType.LMT && order.getLimit()==-1)
		{
			System.out.println("Limit undefined\n");
			return false;
		}
		
		// test if the order condition is acceptable
		if(! this.acceptableOrderCondition.contains(order.getOrderCondition()))
			return false;
		
		// test if the order duration is acceptable
		if(! this.acceptableOrderDuration.contains(order.getOrderDuration()))
			return false;
		
		return true;
	}
	
	public void addQuotedAsset(AbstractAsset asset)
	{
		if(!isQuoted(asset))
		{
			try 
			{
				AbstractMatchingMethod newQuotedAsset;
				try {
					newQuotedAsset = this.matchingMethod.getClass().getDeclaredConstructor(MarketSimModel.class).newInstance(model);
					newQuotedAsset.setAsset(asset);
					newQuotedAsset.setVenue(this);
					this.quotedAssets.add(newQuotedAsset);
				} catch (IllegalArgumentException | InvocationTargetException
						| NoSuchMethodException | SecurityException e) {
					e.printStackTrace();
				}
			}
			catch (InstantiationException | IllegalAccessException e) 
			{
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * @return the currency
	 */
	public Currency getCurrency() {
		return currency;
	}


	/**
	 * @param currency the currency to set
	 */
	public void setCurrency(Currency currency) {
		this.currency = currency;
	}


	public boolean isQuoted(AbstractAsset asset)
	{
		for(int i=0; i<quotedAssets.size();i++)
			if(quotedAssets.get(i).getAssetName().compareTo(asset.getName())==0)
				return true;
		//System.out.println("Asset " + asset.getName() + " not found.\n");
		return false;
	}


	public void addAcceptablePriceType(PriceType priceType) 
	{
		if(! this.acceptablePriceType.contains(priceType))
			this.acceptablePriceType.add(priceType);
	}


	public void addAcceptableOrderCondition(OrderCondition orderCondition) {
		if(! this.acceptableOrderCondition.contains(orderCondition))
			this.acceptableOrderCondition.add(orderCondition);
	}


	public void addAcceptableOrderDuration(OrderDuration orderDuration) {
		if(! this.acceptableOrderDuration.contains(orderDuration))
			this.acceptableOrderDuration.add(orderDuration);
	}
	
	public String getMic()
	{
		return this.mic;
	}
	
	public String toString()
	{
		String res = "";
		for(int i = 0; i< this.quotedAssets.size(); i++)
		{
			res = res + this.quotedAssets.get(i).toString();
		}
		return res;
	}
	
	private AbstractMatchingMethod getQuotedAsset(String name)
	{
		for(int i=0; i< quotedAssets.size(); i++)
			if(quotedAssets.get(i).getAssetName().compareTo(name)==0)
				return quotedAssets.get(i);
		return null;
	}


	public String getName() {
		return name;
	}


	public MarketSimModel getModel() 
	{
		return model;
	}


	public AbstractMatchingMethod getMatchingMethod(String assetName)
	{
		for(int i=0; i<quotedAssets.size(); i++)
			if(quotedAssets.get(i).getAssetName().compareTo(assetName)==0)
				return quotedAssets.get(i);
		return null;
	}


	public void onTick()
	{
		for(int i=0; i<quotedAssets.size();i++)
			quotedAssets.get(i).onTick(model.getDate());
	}


	public boolean removeOrder(Date date, Side side, AbstractAsset asset, Portfolio portfolio, int amount, int price) 
	{
		if(!isQuoted(asset)) return false;
		return getQuotedAsset(asset.getName()).removeOrder(date, side, asset, portfolio, amount, price);
	}


	public LinkedList<AbstractMatchingMethod> getQuotedAssets() {
		return quotedAssets;
	}
}
