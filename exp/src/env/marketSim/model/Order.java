package marketSim.model;

import java.util.Date;

public class Order
{
	//Basic information
	protected AbstractAsset asset;
	protected Side side;
	protected int quantity;
	protected Broker broker;
	protected Portfolio portfolio;
	protected Date placementDate;
	
	// Typing
	protected PriceType priceType;
	protected OrderCondition orderCondition;
	protected OrderDuration orderDuration;
	
	// LMT options
	float limit;
	
	//TODO implement other conditions and durations (GTD)
	
	/**
	 * Semi-default constructor : build a good for day market order with the given parameters
	 * @param asset the asset to exchange
	 * @param side the Side.BID or Side.ASK
	 * @param quantity the amount of assets to exchange
	 * @param broker the broker processing the transaction
	 * @param pf the porfolio which contains the assets to exchange
	 */
	public Order(AbstractAsset asset, Side side, int quantity, Broker broker, Portfolio pf) 
	{
		this(asset, side, quantity,PriceType.MKT,OrderCondition.NONE,OrderDuration.DAY, broker, pf, null);
	}
	
	/**
	 * Precise constructor
	 * @param asset the asset to exchange
	 * @param side the Side.BID or Side.ASK
	 * @param quantity the amount of assets to exchange
	 * @param priceType the method used to fix the price
	 * @param orderCondition the execution conditions of the order
	 * @param orderDuration the time to live of the order
	 * @param broker the broker processing the transaction
	 * @param pf the porfolio which contains the assets to exchange
	 */
	public Order(AbstractAsset asset, 
			Side side, 
			int quantity, 
			PriceType priceType, 
			OrderCondition orderCondition, 
			OrderDuration orderDuration, 
			Broker broker, 
			Portfolio pf,
			Date date)
	{
		this.asset=asset;
		this.side=side;
		this.quantity=quantity;
		this.broker=broker;
		this.portfolio=pf;
		this.priceType=priceType;
		this.orderCondition=orderCondition;
		this.orderDuration=orderDuration;
		this.limit=-1;
		this.placementDate=date;
	}
	
	/**
	 * Copy constructor
	 */
	public Order(Order other)
	{
		this(	other.getAsset(), 
				other.getSide(), 
				other.getQuantity(), 
				other.getPriceType(), 
				other.getOrderCondition(), 
				other.getOrderDuration(), 
				other.getBroker(), 
				other.getPortfolio(),
				other.getPlacementDate());
		this.limit=other.getLimit();
	}
	

	/**
	 * @return the date of placement
	 */
	public Date getPlacementDate() {
		return placementDate;
	}

	/**
	 * @return the asset
	 */
	public AbstractAsset getAsset() {
		return asset;
	}

	/**
	 * @return the side
	 */
	public Side getSide() {
		return side;
	}

	/**
	 * @return the quantity
	 */
	public int getQuantity() {
		return quantity;
	}

	/**
	 * @return the broker
	 */
	public Broker getBroker() {
		return broker;
	}

	/**
	 * @return the portfolio
	 */
	public Portfolio getPortfolio() {
		return portfolio;
	}

	/**
	 * @return the priceType
	 */
	public PriceType getPriceType() {
		return priceType;
	}

	/**
	 * @return the orderCondition
	 */
	public OrderCondition getOrderCondition() {
		return orderCondition;
	}

	/**
	 * @return the orderDuration
	 */
	public OrderDuration getOrderDuration() {
		return orderDuration;
	}

	/**
	 * @return the limit
	 */
	public float getLimit() {
		return limit;
	}

	/**
	 * @param date the date to set
	 */
	public void setPlacementDate(Date date) {
		this.placementDate=date;
	}
	
	/**
	 * @param asset the asset to set
	 */
	public void setAsset(AbstractAsset asset) {
		this.asset = asset;
	}

	/**
	 * @param side the side to set
	 */
	public void setSide(Side side) {
		this.side = side;
	}

	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	/**
	 * @param broker the broker to set
	 */
	public void setBroker(Broker broker) {
		this.broker = broker;
	}

	/**
	 * @param pf the portfolio to set
	 */
	public void setPortfolio(Portfolio portfolio) {
		this.portfolio = portfolio;
	}

	/**
	 * @param priceType the priceType to set
	 */
	public void setPriceType(PriceType priceType) {
		this.priceType = priceType;
	}

	/**
	 * @param orderCondition the orderCondition to set
	 */
	public void setOrderCondition(OrderCondition orderCondition) {
		this.orderCondition = orderCondition;
	}

	/**
	 * @param orderDuration the orderDuration to set
	 */
	public void setOrderDuration(OrderDuration orderDuration) {
		this.orderDuration = orderDuration;
	}

	/**
	 * @param limit the limit to set
	 */
	public void setLimit(float limit) {
		this.limit = limit;
	}
	
	

}