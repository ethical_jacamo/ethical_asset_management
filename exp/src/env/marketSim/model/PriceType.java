/**
 * 
 */
package marketSim.model;

/**
 * @author Nicolas
 *
 */
public enum PriceType
{
	MKT, // Market order
	LMT, // Limit order
	ATO, // At the open 
	ATC  // At the close
}