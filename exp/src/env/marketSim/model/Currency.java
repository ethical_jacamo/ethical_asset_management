/**
 * 
 */
package marketSim.model;

/**
 * @author Nicolas
 *
 */
public class Currency extends AbstractAsset {

	protected Currency(String name, String symbol, String comments) 
	{
		super(name, symbol, comments);
	}

}
