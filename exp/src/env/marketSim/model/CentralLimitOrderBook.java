/**
 * 
 */
package marketSim.model;

import java.util.Date;
import java.util.LinkedList;
import java.util.TreeMap;

/**
 * @author Nicolas
 *
 */
public class CentralLimitOrderBook extends AbstractMatchingMethod{

	protected TreeMap<Integer,LinkedList<Order>> bidSide;
	protected TreeMap<Integer,LinkedList<Order>> askSide;
	
	/**
	 * 
	 */
	public CentralLimitOrderBook(MarketSimModel model)
	{
		super(model);
		bidSide= new TreeMap<Integer,LinkedList<Order>>();
		askSide= new TreeMap<Integer,LinkedList<Order>>();
	}
	
	/**
	 * This method pale the order in the central limit order book, transform it into a limit order and call the brokers in case of matching to fill them fully or partially
	 * @param order the order to place in the central limit order book
	 */
	@Override
	public void placeOrder(Order order)
	{
		order.setPlacementDate(model.getDate());
		if(order.getSide()==Side.ASK) // Case of an ASK order
		{
			if(order.getPriceType()== PriceType.MKT) // Case of a market order : we transform it into a limit order with the market price as a limit
			{
				if(getBestBid()!=-1) order.setLimit(getBestBid());
				else order.setLimit(getBestAsk());
				order.setPriceType(PriceType.LMT);
			}
			if(order.getPriceType()== PriceType.LMT) // Case of a limit order
			{
				while(getBestBid()!=-1.0 && getBestBid()>= order.getLimit()) // While they are matching orders
				{
					System.out.println("BestBid is " + ((int)(getBestBid()*1000)));
					Order bestBidOrder=bidSide.get((int) (getBestBid()*1000)).getFirst();// TODO Remove dat bug
					if(bestBidOrder.getQuantity()==order.getQuantity()) // We fill completely both coming order and clob order
					{
						diffuseOrderExecutionInformation(new Order(order), new Order(bestBidOrder), order.getQuantity()); // diffuse information to subscribers
						assetDataSets.get(assetDataSets.size()-1).putMatching(order.getQuantity(), getBestBid());
						bestBidOrder.getBroker().fillOrder(bestBidOrder, bestBidOrder.getQuantity(), this.getVenue().getCurrency(), getBestBid()); // fill the best bid
						order.getBroker().fillOrder(order, order.getQuantity(), this.getVenue().getCurrency(), getBestBid()); // fill the order
						this.bidSide.get((int)(getBestBid()*1000)).removeFirst(); // remove the best bid
						if(bidSide.get((int) (getBestBid()*1000)).size()==0) this.bidSide.remove((int) (getBestBid()*1000));
						return;
					}
					if(bestBidOrder.getQuantity()>order.getQuantity()) // We fill coming order completely and partially clob order
					{
						diffuseOrderExecutionInformation(new Order(order), new Order(bestBidOrder), order.getQuantity()); // diffuse information to subscribers
						assetDataSets.get(assetDataSets.size()-1).putMatching(order.getQuantity(), getBestBid());
						bestBidOrder.getBroker().fillOrder(bestBidOrder, order.getQuantity(), this.getVenue().getCurrency(), getBestBid()); // fill partially the best bid
						order.getBroker().fillOrder(order, order.getQuantity(), this.getVenue().getCurrency(), getBestBid()); // fill the order
						return;
					}
					// We fill completely CLOB order and partially coming order
					int size =  bestBidOrder.getQuantity();
					diffuseOrderExecutionInformation(new Order(order), new Order(bestBidOrder), size); // diffuse information to subscribers
					assetDataSets.get(assetDataSets.size()-1).putMatching(bestBidOrder.getQuantity(),getBestBid());
					bestBidOrder.getBroker().fillOrder(bestBidOrder, size, this.getVenue().getCurrency(), getBestBid()); // fill the best bid
					this.bidSide.get((int)(getBestBid()*1000)).removeFirst();
					order.getBroker().fillOrder(order, size, this.getVenue().getCurrency(), getBestBid()); // fill partially the order
					if(bidSide.get((int) (getBestBid()*1000)).size()==0) 
						this.bidSide.remove((int) (getBestBid()*1000)); // remove the list of orders for this value if empty
				}
				float bestBid = this.getBestBid();
				if(bestBid==-1.0 || bestBid < order.getLimit()) // if there is no order to match with, we put the order into the CLOB
				{
					order.setPlacementDate(model.getDate());
					diffuseOrderAdditionInformation(new Order(order)); // we diffuse the information about the addition of an order to the subscribers
					if(!this.askSide.containsKey((int) (order.getLimit()*1000)))
						this.askSide.put((int) (order.getLimit()*1000), new LinkedList<Order> ());
					this.askSide.get((int) (order.getLimit()*1000)).add(order);						
				}
			}
		}
		else // Case of a BID order (buy)
		{
			if(order.getPriceType()== PriceType.MKT) // Case of a market order : we transform it into a limit order with the market price as a limit
			{
				if(getBestAsk()!=-1) order.setLimit(getBestAsk());
				else order.setLimit(getBestBid());
				order.setPriceType(PriceType.LMT);
			}
			if(order.getPriceType()== PriceType.LMT) // Case of a limit order
			{
				while(getBestAsk()!=-1.0 && getBestAsk()<= order.getLimit()) // While they are matching orders
				{
					System.out.println("BestAsk is " + ((int)(getBestAsk()*1000)));
					Order bestAskOrder=askSide.get((int) (getBestAsk()*1000)).getFirst();
					if(bestAskOrder.getQuantity()==order.getQuantity())
					{
						diffuseOrderExecutionInformation(new Order(order), new Order(bestAskOrder), order.getQuantity()); // diffuse information to subscribers
						assetDataSets.get(assetDataSets.size()-1).putMatching(order.getQuantity(), getBestAsk());
						bestAskOrder.getBroker().fillOrder(bestAskOrder, bestAskOrder.getQuantity(), this.getVenue().getCurrency(), getBestAsk()); // fill the best ask
						order.getBroker().fillOrder(order, order.getQuantity(), this.getVenue().getCurrency(), getBestAsk()); // fill the order
						this.askSide.get((int)(getBestAsk()*1000)).removeFirst(); // remove the best ask TODO il y aurait un pb ici ?
						if(askSide.get((int) (getBestAsk()*1000)).size()==0) this.askSide.remove((int) (getBestAsk()*1000));
						return;
					}
					if(bestAskOrder.getQuantity()>order.getQuantity())
					{
						diffuseOrderExecutionInformation(new Order(order), new Order(bestAskOrder), order.getQuantity()); // diffuse information to subscribers
						assetDataSets.get(assetDataSets.size()-1).putMatching(order.getQuantity(), getBestAsk());
						bestAskOrder.getBroker().fillOrder(bestAskOrder, order.getQuantity(), this.getVenue().getCurrency(), getBestAsk()); // fill partially the best ask
						order.getBroker().fillOrder(order, order.getQuantity(), this.getVenue().getCurrency(), getBestAsk()); // fill the order
						return;
					}
					int size =  bestAskOrder.getQuantity();
					diffuseOrderExecutionInformation(new Order(order), new Order(bestAskOrder), size); // diffuse information to subscribers
					assetDataSets.get(assetDataSets.size()-1).putMatching(bestAskOrder.getQuantity(), getBestAsk());
					bestAskOrder.getBroker().fillOrder(bestAskOrder, size, this.getVenue().getCurrency(), getBestAsk()); // fill the best ask
					askSide.get((int) (getBestAsk()*1000)).pop();
					order.getBroker().fillOrder(order, size, this.getVenue().getCurrency(), getBestAsk()); // fill partially the order
					if(askSide.get((int) (getBestAsk()*1000)).size()==0) this.askSide.remove((int) (getBestAsk()*1000)); // remove the list of orders for this value if empty
				}
				float bestAsk = this.getBestAsk();
				if(bestAsk==-1.0 || bestAsk < order.getLimit()) // if there is no order to match with, we put the order into the CLOB
				{
					order.setPlacementDate(model.getDate());
					diffuseOrderAdditionInformation(new Order(order)); // we diffuse the information about the addition of an order to the subscribers
					if(!this.bidSide.containsKey((int) (order.getLimit()*1000)))
						this.bidSide.put((int) (order.getLimit()*1000), new LinkedList<Order> ());
					this.bidSide.get((int) (order.getLimit()*1000)).add(order);						
				}
			}
		}
	}

	public float getBestBid()
	{
		if(this.bidSide.isEmpty())
			return (float) -1.0;
		return (float) (((float)this.bidSide.lastKey())/1000.0);
	}
	
	public float getBestAsk()
	{
		if(this.askSide.isEmpty())
			return (float) -1.0;
		return (float) (((float)this.askSide.firstKey())/1000.0);
	}
	
	public String toString()
	{
		if(this.askSide.size()==0 && this.bidSide.size()==0)
			return " CLOB of " + this.asset.getName() + " is empty\n";
		int min;
		int max;
		String out="Current state of the CLOB for the asset " + this.asset.getName() + "\n";
		if(this.bidSide.size()!=0 && (this.askSide.size()==0 || this.bidSide.firstKey() <=this.askSide.firstKey()))
			min=this.bidSide.firstKey();
		else
			min=this.askSide.firstKey();
		if(this.bidSide.size()!=0 && (this.askSide.size()==0 || this.bidSide.lastKey() > this.askSide.lastKey()))
			max=this.bidSide.lastKey();
		else
			max=this.askSide.lastKey();
		for(int i=min; i<=max; i++)
		{
			if(this.bidSide.containsKey(i) || this.askSide.containsKey(i))
			{
				out=out+ ((float)i)/1000.0 + " : ";
				if(this.bidSide.containsKey(i))
					for(int j=0; j<this.bidSide.get(i).size(); j++)
						out = out + " | B" + this.bidSide.get(i).get(j).getQuantity();
				if(this.askSide.containsKey(i))
					for(int j=0; j<this.askSide.get(i).size(); j++)
						out = out + " | S" + this.askSide.get(i).get(j).getQuantity();
				out=out + "\n";
			}
		}
		return out;
	}
	
	public boolean removeOrder(Date date, Side side, AbstractAsset asset, Portfolio portfolio, int amount, int price)
	{
		if(side==Side.BID) 
			System.out.println("I am looking for ("+date+",bid,"+asset.getName()+","+portfolio.getName()+","+amount+","+price+")");
		else
			System.out.println("I am looking for  ("+date+",ask,"+asset.getName()+","+portfolio.getName()+","+amount+","+price+")");
		
		if(side==Side.BID && bidSide.get(price) != null && !bidSide.get(price).isEmpty())
		{
			for(int i=0; i<bidSide.get(price).size(); i++)
			{
				if(bidSide.get(price).get(i).getPortfolio() == portfolio 
						&& bidSide.get(price).get(i).getQuantity()==amount 
						&& (int)(bidSide.get(price).get(i).getLimit()*1000.0) ==price)
				{
					diffuseOrderCancelationInformation(new Order(bidSide.get(price).get(i)));
					bidSide.get(price).remove(i);
					if(bidSide.get(price).size()==0) bidSide.remove(price);
					//System.out.println("J'ai trouvé !");
					return true;
				}	
			}
		}
		if(askSide.get(price) != null && !askSide.get(price).isEmpty())
		{
			System.out.println("They are ask orders at this price !");
			for(int i=0; i<askSide.get(price).size(); i++)
			{
				System.out.println("The " + i + "th is a Qty of " + askSide.get(price).get(i).getQuantity() + " since " + askSide.get(price).get(i).getPlacementDate());
				if(askSide.get(price).get(i).getPortfolio()==portfolio && askSide.get(price).get(i).getQuantity()==amount)
				{
					diffuseOrderCancelationInformation(new Order(askSide.get(price).get(i)));
					askSide.get(price).remove(i);
					if(askSide.get(price).size()==0) askSide.remove(price);
					return true;
				}
				System.out.println("But it's not the good one");
			}
		}
		System.out.println("Warning : order to cancel not found.");
		return false;
	}

	public float getLastClose()
	{
		if(assetDataSets.get(assetDataSets.size()-1).getX_close()!=-1.0)
			return assetDataSets.get(assetDataSets.size()-1).getX_close();
		if(getBestBid()!=-1.0)
			return getBestBid();
		if(getBestAsk()!=-1.0)
			return getBestAsk();
		return (float) 0.0;
	}
}