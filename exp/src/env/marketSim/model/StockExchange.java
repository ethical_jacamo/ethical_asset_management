package marketSim.model;

public class StockExchange extends AbstractTradingVenue {

	/**
	 * Constructor
	 * @param model
	 * @param matchingMethod
	 * @param name
	 * @param country
	 * @param city
	 * @param mic
	 * @param currency
	 * @param comments
	 */
	public StockExchange(MarketSimModel model, 
			AbstractMatchingMethod matchingMethod, 
			String name, 
			String country, 
			String city, 
			String mic, 
			Currency currency, 
			String comments)
	{
		super(model, matchingMethod, name, country, city, mic, currency, comments);
	}
	
	public StockExchange(MarketSimModel model, String matching, String name, String country, String city, String mic, Currency currency, String comments)
	{
		super(model, new CentralLimitOrderBook(model), name, country, city, mic, currency, comments);
		if(!matching.equals(new String("CLOB")))
		{
			//.print("ERROR : Unknown StockExchange matching method '" + matching +"' for venue '" + name + "'. Central Limit Order Book assigned by default.");
		}
	}
	
	/**
	 * Semi-default constructor matching method is a CLOB by default
	 * @param model
	 * @param name
	 * @param country
	 * @param city
	 * @param mic
	 * @param currency
	 * @param comments
	 */
	public StockExchange(MarketSimModel model, String name, String country, String city, String mic, Currency currency, String comments)
	{
		super(model, new CentralLimitOrderBook(model), name, country, city, mic, currency, comments);
	}

}
