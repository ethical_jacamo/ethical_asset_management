/**
 * 
 */
package marketSim.model;

/**
 * @author Nicolas
 *
 */
public enum OrderDuration
{
	DAY,GTD,GTC
}
