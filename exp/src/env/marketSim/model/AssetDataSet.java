package marketSim.model;

import java.util.Date;

public class AssetDataSet {

	protected long n; // quantity of matchings 
	protected long v; // volume of exchanges
	protected float x_min; // minimal price
	protected float x_max; // maximal price
	protected float x_open; // price on opening
	protected float x_close; // price on close
	protected float average; // average price
	protected float doubleLengthAverage; // average on the double of the normal period
	protected float var; // variance of prices
	protected float std_dev; // standard deviation
	protected float intensity; // intensity
	protected float bollingerUp; // bollinger up
	

	/**
	 * @return the doubleLengthAverage
	 */
	public float getDoubleLengthAverage() {
		return doubleLengthAverage;
	}

	/**
	 * @param doubleLengthAverage the doubleLengthAverage to set
	 */
	public void setDoubleLengthAverage(float doubleLengthAverage) {
		this.doubleLengthAverage = doubleLengthAverage;
	}

	/**
	 * @return the bollingerUp
	 */
	public float getBollingerUp() {
		return bollingerUp;
	}

	/**
	 * @return the bollingerDown
	 */
	public float getBollingerDown() {
		return bollingerDown;
	}

	/**
	 * @param bollingerUp the bollingerUp to set
	 */
	public void setBollingerUp(float bollingerUp) {
		this.bollingerUp = bollingerUp;
	}

	/**
	 * @param bollingerDown the bollingerDown to set
	 */
	public void setBollingerDown(float bollingerDown) {
		this.bollingerDown = bollingerDown;
	}

	protected float bollingerDown; // bollinger down

	
	/**
	 * @return the intensity
	 */
	public float getIntensity() {
		return intensity;
	}

	/**
	 * @param intensity the intensity to set
	 */
	public void setIntensity(float intensity) {
		this.intensity = intensity;
	}

	protected Date dateOfBegin;
	protected Date dateOfEnd;
	
	
	
	/**
	 * Default constructor
	 * @param date
	 */
	public AssetDataSet(Date date)
	{
		this((long)0,(long)0,(float)-1.0,(float)-1.0,(float)-1.0,(float)-1.0,(float)-1.0,(float)-1.0,(float)-1.0,date,date);
	}
	
	public AssetDataSet(long n, long v, float x_min, float x_max, float x_open, float x_close, float average, float variance, float std_dev, Date dateOfBegin, Date dateOfEnd)
	{
		this.n=n;
		this.v=v;
		this.x_min=x_min;
		this.x_max=x_max;
		this.x_open=x_open;
		this.x_close=x_close;
		this.average=average;
		this.var=variance;
		this.std_dev=std_dev;
		this.dateOfBegin=dateOfBegin;
		this.dateOfEnd=dateOfEnd;
	}
	
	/**
	 * Copy constructor
	 * @param other
	 */
	public AssetDataSet(AssetDataSet other)
	{
		this.n=other.n;
		this.v=other.v;
		this.x_min=other.x_min;
		this.x_max=other.x_max;
		this.x_open=other.x_open;
		this.x_close=other.x_close;
		this.average=other.average;
		this.var=other.var;
		this.std_dev=other.std_dev;
		this.dateOfBegin=other.dateOfBegin;
		this.dateOfEnd=other.dateOfEnd;
		
	}
	
	/**
	 * @return the dateOfBegin
	 */
	public Date getDateOfBegin() {
		return dateOfBegin;
	}

	/**
	 * @return the dateOfEnd
	 */
	public Date getDateOfEnd() {
		return dateOfEnd;
	}

	/**
	 * @param dateOfBegin the dateOfBegin to set
	 */
	public void setDateOfBegin(Date dateOfBegin) {
		this.dateOfBegin = dateOfBegin;
	}

	/**
	 * @param dateOfEnd the dateOfEnd to set
	 */
	public void setDateOfEnd(Date dateOfEnd) {
		this.dateOfEnd = dateOfEnd;
	}

	public AssetDataSet aggregateWith(AssetDataSet other)
	{
		if(this.dateOfBegin.compareTo(other.dateOfEnd)!=0 && this.dateOfEnd.compareTo(other.dateOfBegin)!=0)
			return null;
		
		if(this.dateOfBegin.compareTo(other.dateOfEnd)==0) // this one is just before the other one
			return new AssetDataSet(this.n+other.n,
					this.v+other.v,
					Math.min(this.x_min,other.x_min),
					Math.max(this.x_max, other.x_max),
					this.x_open,
					other.x_close,
					(((float)this.v)*this.average+((float)other.v)*other.average)/((float)(this.v+other.v)),
					(((float)this.n)*this.var+((float)other.n)*other.var)/((float)(this.n+other.n)),
					(float)Math.sqrt((((float)this.n)*this.var+((float)other.n)*other.var)/((float)(this.n+other.n))),
					this.dateOfBegin,
					other.dateOfEnd);
		return new AssetDataSet(this.n+other.n,
					this.v+other.v,
					Math.min(this.x_min,other.x_min),
					Math.max(this.x_max, other.x_max),
					other.x_open,
					this.x_close,
					(((float)this.v)*this.average+((float)other.v)*other.average)/((float)(this.v+other.v)),
					(((float)this.n)*this.var+((float)other.n)*other.var)/((float)(this.n+other.n)),
					(float)Math.sqrt((((float)this.n)*this.var+((float)other.n)*other.var)/((float)(this.n+other.n))),
					other.dateOfBegin,
					this.dateOfEnd);
	}
	
	/**
	 * @return the n
	 */
	public long getN() {
		return n;
	}
	/**
	 * @return the v
	 */
	public long getV() {
		return v;
	}
	/**
	 * @return the x_min
	 */
	public float getX_min() {
		return x_min;
	}
	/**
	 * @return the x_max
	 */
	public float getX_max() {
		return x_max;
	}
	/**
	 * @return the x_open
	 */
	public float getX_open() {
		return x_open;
	}
	/**
	 * @return the x_close
	 */
	public float getX_close() {
		return x_close;
	}
	/**
	 * @return the average
	 */
	public float getAverage() {
		return average;
	}
	/**
	 * @return the variance
	 */
	public float getVariance() {
		return var;
	}
	/**
	 * @return the std_dev
	 */
	public float getStd_dev() {
		return std_dev;
	}
	/**
	 * @param n the n to set
	 */
	public void setN(long n) {
		this.n = n;
	}
	/**
	 * @param v the v to set
	 */
	public void setV(long v) {
		this.v = v;
	}
	/**
	 * @param x_min the x_min to set
	 */
	public void setX_min(float x_min) {
		this.x_min = x_min;
	}
	/**
	 * @param x_max the x_max to set
	 */
	public void setX_max(float x_max) {
		this.x_max = x_max;
	}
	/**
	 * @param x_open the x_open to set
	 */
	public void setX_open(float x_open) {
		this.x_open = x_open;
	}
	/**
	 * @param x_close the x_close to set
	 */
	public void setX_close(float x_close) {
		this.x_close = x_close;
	}
	/**
	 * @param average the average to set
	 */
	public void setAverage(float average) {
		this.average = average;
	}
	/**
	 * @param variance the variance to set
	 */
	public void setVariance(float variance) {
		this.var = variance;
	}
	/**
	 * @param std_dev the std_dev to set
	 */
	public void setStd_dev(float std_dev) {
		this.std_dev = std_dev;
	}

	public void putMatching(int volume, float price)
	{	//TODO Calculate average, variance and standard deviation
		n++; // increase number of matchings
		v+=volume; // increase total volume
		if(x_open==-1.0) x_open=price;
		x_close=price;
		if(x_min > price || x_min==-1.0) x_min=price;
		if(x_max < price) x_max=price;
	}
	
	public void setDefaultValue(float value)
	{
		x_min=value;
		x_max=value;
		x_open=value;
		x_close=value;
		average=value;
		var=(float) 0.0;
		std_dev=(float) 0.0;
	}
}
