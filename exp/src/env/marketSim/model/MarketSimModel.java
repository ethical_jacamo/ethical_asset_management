/**
 * 
 */
package marketSim.model;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Observable;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import cartago.ArtifactId;
import marketSim.controller.MarketSimArtifact;
import marketSim.controller.StatisticsOutputByAsset;
import marketSim.controller.StatisticsOutputByPortfolio;

/**
 * @author Nicolas
 * This class extends the Cartago Environment to define the environment "behaviour".
 */
public class MarketSimModel extends Observable //extends Environment
{
	protected ArrayList<MarketSimArtifact> connectedAgents = new ArrayList<MarketSimArtifact>();
	protected ArrayList<AbstractAsset> assets = new ArrayList<AbstractAsset>();
	protected ArrayList<AbstractTradingVenue> venues = new ArrayList<AbstractTradingVenue>();
	protected ArrayList<Broker> brokers = new ArrayList<Broker>();
	protected ArrayList<Portfolio> portfolios = new ArrayList<Portfolio>();
	
	protected MarketSimArtifact artifact;
	
	protected Date date=null;
	protected Date previousDate=null;
	protected ArrayList<StatisticsOutputByPortfolio> statsPortfolios = new ArrayList<StatisticsOutputByPortfolio>();
	protected ArrayList<StatisticsOutputByAsset> statsAssets = new ArrayList<StatisticsOutputByAsset>();
	
	protected boolean logImages=false;
	
	/**
	 * Default constructor
	 * @param marketSimArtifact the artifact of the simulator
	 */
	public MarketSimModel(MarketSimArtifact marketSimArtifact)
	{
		this.artifact=marketSimArtifact;
	}
	
	/**
	 * Parse and load the configuration from an xml file (the view's selectedDataFile).
	 * 
	 * Inspired from Florian Benavent's master thesis 2015 HighFrequencyTrading.java class 
	 */
	public void deserialize(File selectedDataFile)
	{
		artifact.print("loading model\n");
		try
		{
			// Open file and build document
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(selectedDataFile);
			doc.getDocumentElement().normalize();
			
			// Collect parameters about the simulation itself
			NodeList nList = doc.getElementsByTagName("simulation");
			for (int temp = 0; temp < nList.getLength(); temp++)
			{
				Node nNode = nList.item(temp);
				if (nNode.getNodeType() == Node.ELEMENT_NODE) 
				{
					Element eElement = (Element) nNode;
					String durationString = eElement.getAttribute("duration");
					String logImagesString = eElement.getAttribute("logImages");
					int duration=Integer.parseInt(durationString);
					artifact.sendDuration(duration);
					if(logImagesString.equals("yes"))
						logImages=true;
				}
			}
			
			// Instantiate assets
			nList = doc.getElementsByTagName("asset");
			for (int temp = 0; temp < nList.getLength(); temp++)
			{
				Node nNode = nList.item(temp);
				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;
					String typeString = eElement.getAttribute("type");
					String nameString = eElement.getAttribute("name");
					String symbolString = eElement.getAttribute("symbol");
					String commentsString = eElement.getAttribute("comments");
					switch(typeString)
					{
						case "STOCK": // Creation of an Equity and addition into assets
							String isinString = eElement.getAttribute("isin");
							String sectorString = eElement.getAttribute("sector");
							String subsectorString = eElement.getAttribute("subsector");
							this.assets.add(new Equity(nameString, symbolString, isinString, sectorString, subsectorString, commentsString));
							artifact.print("Stock asset '" + nameString +"' added\n");
							artifact.addAsset(nameString);
							break;
						case "CURRENCY": // Creation of a currency and addition into assets
							this.assets.add(new Currency(nameString, symbolString, commentsString));
							artifact.print("Currency '" + nameString +"' added\n");
							artifact.addAsset(nameString);
							break;
						default :
							artifact.print("WARNING asset type '" + typeString + "' unknown or not implemented yet for asset '" + nameString + "'.\n" );
					}
				}
			}
			
			// Instantiate portfolios
						NodeList portfolioNodeList = doc.getElementsByTagName("portfolio");
						for (int temp = 0; temp < portfolioNodeList.getLength(); temp++)
						{
							Node portfolioNode = portfolioNodeList.item(temp);
							if (portfolioNode.getNodeType() == Node.ELEMENT_NODE)
							{
								Element portfolioElement = (Element) portfolioNode;
								String nameString = portfolioElement.getAttribute("name");
								String holderString = portfolioElement.getAttribute("holder");
								String brokerString = portfolioElement.getAttribute("broker");
								Portfolio portfolio =new Portfolio(nameString, holderString, brokerString);
								
								// Collect assets of this portfolio
								NodeList assetList = portfolioElement.getElementsByTagName("ownedAsset");
								for(int tttemp = 0; tttemp < assetList.getLength(); tttemp++)
								{
									Node assetNode = assetList.item(tttemp);
									if (assetNode.getNodeType() == Node.ELEMENT_NODE)						
									{
										Element assetElement = (Element) assetNode;
										String isinString = assetElement.getAttribute("isin");
										String volumeString = assetElement.getAttribute("volume");

										if(getAssetByIsin(isinString) != null)
											portfolio.addAssets(getAssetByIsin(isinString) , Integer.parseInt(volumeString));
										else
										{
											String ownedAssetNameString = assetElement.getAttribute("name");
											if(getAssetByName(ownedAssetNameString) != null)
												portfolio.addAssets(getAssetByName(ownedAssetNameString) , Integer.parseInt(volumeString));
											else
												artifact.print("WARNING : Asset '" + isinString +"' not found\n");
										}
									}
								}
								this.portfolios.add(portfolio);
								artifact.print("Portfolio '" + nameString +"' added\n");
								artifact.print(portfolio.toString());
							}
						}

			
			// Instantiate venues
			nList = doc.getElementsByTagName("market");
			for (int temp = 0; temp < nList.getLength(); temp++)
			{
				Node nNode = nList.item(temp);
				if (nNode.getNodeType() == Node.ELEMENT_NODE)
				{
					Element marketElement = (Element) nNode;
					String typeString = marketElement.getAttribute("type");
					String nameString = marketElement.getAttribute("name");
					String micString = marketElement.getAttribute("mic");
					String commentsString = marketElement.getAttribute("comments");
					String countryString = marketElement.getAttribute("country");
					String cityString = marketElement.getAttribute("city");
					String currencyString = marketElement.getAttribute("currency");
					String matchingString = marketElement.getAttribute("matching");
					switch(typeString)
					{
						case "STOCK EXCHANGE": // Creation of a Stock Exchange market and addition into venues
							Currency currency=getCurrencyBySymbol(currencyString);
							this.venues.add(new StockExchange(this, matchingString, nameString, countryString, cityString, micString, currency, commentsString));
							artifact.print("Stock Exchange '" + nameString +"' added\n");
							artifact.addVenue(nameString);
							break;
						default :
							artifact.print("WARNING market type '" 
									+ typeString 
									+ "' unknown or not implemented yet for market '" 
									+ nameString + "'.\n" );
					}
					
					// Instantiate acceptable price types
					NodeList acceptablePriceTypesList = marketElement.getElementsByTagName("acceptablePriceType");
					for(int ttemp = 0; ttemp < acceptablePriceTypesList.getLength(); ttemp++)
					{
						Node acceptableOrdersNode = acceptablePriceTypesList.item(ttemp);
						if (acceptableOrdersNode.getNodeType() == Node.ELEMENT_NODE)
						{
							Element acceptableOrderElement = (Element) acceptableOrdersNode;
							String acceptablePriceTypeString = acceptableOrderElement.getAttribute("type");
							switch(acceptablePriceTypeString)
							{
								case "MKT": // Addition of the "Market" prices into the list of acceptable prices for this market
									artifact.print("Market orders are accepted\n");
									getVenueByMic(micString).addAcceptablePriceType(PriceType.MKT);
									break;
								case "LMT": // Addition of the "Limit" prices into the list of acceptable prices for this market
									artifact.print("Limit order are accepted\n");
									getVenueByMic(micString).addAcceptablePriceType(PriceType.LMT);
									break;
									// TODO implement ATO and ATC
								default :
									artifact.print("WARNING '" 
											+ acceptablePriceTypeString 
											+ "' price type unknown.\n" );
							}
						}
					}
					
					// Instantiate acceptable order conditions
					NodeList acceptableConditionsList = marketElement.getElementsByTagName("acceptableOrderCondition");
					for(int ttemp = 0; ttemp < acceptableConditionsList.getLength(); ttemp++)
					{
						Node acceptableConditionsNode = acceptableConditionsList.item(ttemp);
						if (acceptableConditionsNode.getNodeType() == Node.ELEMENT_NODE)
						{
							Element acceptableConditionElement = (Element) acceptableConditionsNode;
							String acceptableConditionTypeString = acceptableConditionElement.getAttribute("type");
							switch(acceptableConditionTypeString)
							{
								case "NONE": // Addition of the no-condition orders into the list of acceptable conditions for this market
									artifact.print("No-condition orders are accepted\n");
									getVenueByMic(micString).addAcceptableOrderCondition(OrderCondition.NONE);
									break;
									// TODO implement IOC, FOK, STOP, AON, MO and MF
								default :
									artifact.print("WARNING '" 
											+ acceptableConditionTypeString 
											+ "' condition type unknown.\n" );
							}
						}
					}
					
					// Instantiate acceptable order durations
					NodeList acceptableDurationList = marketElement.getElementsByTagName("acceptableOrderDuration");
					for(int ttemp = 0; ttemp < acceptableDurationList.getLength(); ttemp++)
					{
						Node acceptableDurationNode = acceptableDurationList.item(ttemp);
						if (acceptableDurationNode.getNodeType() == Node.ELEMENT_NODE)
						{
							Element acceptableDurationElement = (Element) acceptableDurationNode;
							String acceptableDurationTypeString = acceptableDurationElement.getAttribute("type");
							switch(acceptableDurationTypeString)
							{
								case "DAY": // Addition of the "Good For Day" orders into the list of acceptable orders for this market
									artifact.print("Good for Day orders are accepted\n");
									getVenueByMic(micString).addAcceptableOrderDuration(OrderDuration.DAY);
									break;
								case "GTC": // Addition of the "Good till Canceled" orders into the list of acceptable orders for this market
									artifact.print("Good till canceled orders are accepted\n");
									getVenueByMic(micString).addAcceptableOrderDuration(OrderDuration.GTC);
									break;
									// TODO implement GTD and GTC
								default :
									artifact.print("WARNING '" 
											+ acceptableDurationTypeString 
											+ "' duration type unknown.\n" );
							}
						}
					}
				}
			} // End of "market" parsing
			
			// Instantiate brokers
			nList = doc.getElementsByTagName("broker");
			for (int temp = 0; temp < nList.getLength(); temp++)
			{
				Node nNode = nList.item(temp);
				if (nNode.getNodeType() == Node.ELEMENT_NODE)
				{
					Element brokerElement = (Element) nNode;
					String nameString = brokerElement.getAttribute("name");
					String pipsString = brokerElement.getAttribute("pips");
					String pipsCurrencyString = brokerElement.getAttribute("pipsCurrency");
					Broker broker =new Broker(this,nameString, Float.parseFloat(pipsString), this.getCurrencyBySymbol(pipsCurrencyString));
					
					// Collect any market where the broker is authorized to trade
					NodeList authorizedMarketList = brokerElement.getElementsByTagName("authorizedMarket");
					for(int tttemp = 0; tttemp < authorizedMarketList.getLength(); tttemp++)
					{
						Node authorizedMarketNode = authorizedMarketList.item(tttemp);
						if (authorizedMarketNode.getNodeType() == Node.ELEMENT_NODE)						
						{
							Element authorizedMarketElement = (Element) authorizedMarketNode;
							String micString = authorizedMarketElement.getAttribute("mic");
							artifact.print("Broker '" + nameString +"' authorized on " + micString + "\n");
							broker.addMarket(getVenueByMic(micString));
						}
					}
					this.brokers.add(broker);
					artifact.print("Broker '" + nameString +"' added\n");
				}
			}

			// Instantiate quoted assets in their market
			NodeList quotedAssetsList = doc.getElementsByTagName("quotedAsset");
			for(int ttemp = 0; ttemp < quotedAssetsList.getLength(); ttemp++)
			{
				Node quotedAssetsNode = quotedAssetsList.item(ttemp);
				if (quotedAssetsNode.getNodeType() == Node.ELEMENT_NODE)
				{
					Element quotedAssetElement = (Element) quotedAssetsNode;
					String quotedAssettypeString = quotedAssetElement.getAttribute("type");
					String isinString = quotedAssetElement.getAttribute("isin");
					String micString = quotedAssetElement.getAttribute("market");
					switch(quotedAssettypeString)
					{
						case "STOCK": // Creation of a matchingMethod for a stock option
							AbstractSecurity  sec= this.getAssetByIsin(isinString);
							if(sec==null)
								artifact.print("Stock option '" + isinString +"' not found.\n");
							else
							{
								getVenueByMic(micString).addQuotedAsset(sec);
								artifact.addQuotedAsset(getAssetByIsin(isinString).getName(), getVenueByMic(micString).getName());
								artifact.print("Stock option '" 
										+ isinString
										+"' now quoted on " 
										+ micString + "\n");
							}
							break;
							// TODO implement BONDS and CURRENCY
						default :
							artifact.print("WARNING quoted asset '" 
									+ quotedAssettypeString 
									+ "' unknown or not implemented yet.\n" );
					}
							
					// Instantiate each order of this quoted asset
					NodeList orderList = quotedAssetElement.getElementsByTagName("order");
					for(int tttemp = 0; tttemp < orderList.getLength(); tttemp++)
					{
						Node orderNode = orderList.item(tttemp);
						if (orderNode.getNodeType() == Node.ELEMENT_NODE)						
						{
							Element orderElement = (Element) orderNode;
							String priceTypeString = orderElement.getAttribute("priceType");
							String orderConditionString = orderElement.getAttribute("orderCondition");
							String orderDurationString = orderElement.getAttribute("orderDuration");
							String sideString = orderElement.getAttribute("side");
							String volumeString = orderElement.getAttribute("volume");
							String portfolioString = orderElement.getAttribute("portfolio");
							String brokerString = orderElement.getAttribute("broker");
							PriceType priceType=PriceType.MKT; // Default price type
							switch(priceTypeString)
							{
								case "MKT":
									priceType=PriceType.MKT;
									break;
								case "LMT":
									priceType=PriceType.LMT;
									break;
								case "ATO":
									priceType=PriceType.ATO;
									break;
								case "ATC":
									priceType=PriceType.ATC;
									break;
								default :
									artifact.print("WARNING order price type '" + priceTypeString + "' unknown or not implemented yet.\n" );
							}
							OrderCondition orderCondition=OrderCondition.NONE; // Default order Condition
							switch(orderConditionString)
							{
								case "NONE":
									orderCondition=OrderCondition.NONE;
									break;
								case "IOC":
									orderCondition=OrderCondition.IOC;
									break;
								case "FOK":
									orderCondition=OrderCondition.FOK;
									break;
								case "STOP":
									orderCondition=OrderCondition.STOP;
									break;
								case "AON":
									orderCondition=OrderCondition.AON;
									break;
								case "MO":
									orderCondition=OrderCondition.MO;
									break;
								case "MF":
									orderCondition=OrderCondition.MF;
									break;
								default :
									artifact.print("WARNING order condition '" + orderConditionString + "' unknown or not implemented yet.\n" );
							}
							OrderDuration orderDuration=OrderDuration.GTC; // Default duration
							switch(orderDurationString)
							{
								case "DAY":
									orderDuration=OrderDuration.DAY;
									break;
								case "GTD":
									orderDuration=OrderDuration.GTD;
									break;
								case "GTC":
									orderDuration=OrderDuration.GTC;
									break;
								default :
									artifact.print("WARNING order duration '" + orderDurationString + "' unknown or not implemented yet.\n" );
							}
							Side orderSide=Side.ASK; // Default side
							switch(sideString)
							{
								case "ASK":
									orderSide=Side.ASK;
									break;
								case "BID":
									orderSide=Side.BID;
									break;
								default :
									artifact.print("WARNING order side '" + sideString + "' unknown or not implemented yet.\n" );
							}
							if(getPortfolioByName(portfolioString)==null) 
								artifact.print("WARNING : portfolio " + portfolioString + " not found.\n");
							Order order=new Order(
									this.getAssetByIsin(isinString),
									orderSide,
									Integer.parseInt(volumeString),
									priceType,
									orderCondition,
									orderDuration,
									this.getBrokerByName(brokerString),
									getPortfolioByName(portfolioString),
									date);
							if(priceType==PriceType.LMT)
								order.setLimit(Float.parseFloat(orderElement.getAttribute("limit")));
							
							if(! getVenueByMic(micString).placeOrder(order))
								artifact.print("WARNING : Impossible to place order.\n");
						}
					}
				}
			} // End of "quotedAsset" parsing
			
		} 
		catch (ParserConfigurationException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (SAXException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		catch (IOException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public AbstractAsset getAssetByName(String nameString) 
	{
		for(int i=0; i<this.assets.size(); i++)
		{
			if((this.assets.get(i)).getName().equals(nameString))
				return this.assets.get(i);
		}
		return null;
	}

	public String toString()
	{
		String res = "";
		for(int i = 0; i< this.venues.size(); i++)
		{
			res = res + this.venues.get(i).toString();
		}
		return res;
	}

	private AbstractSecurity getAssetByIsin(String isin)
	{
		for(int i=0; i<this.assets.size(); i++)
		{
			if(!AbstractSecurity.class.isInstance(this.assets.get(i)))
			{
				continue;
			}
			if(((AbstractSecurity) this.assets.get(i)).getIsin().equals(isin))
				return (AbstractSecurity) this.assets.get(i);
		}
		return null;
	}
	
	private Currency getCurrencyBySymbol(String symbol)
	{
		for(int i=0; i<this.assets.size(); i++)
		{
			if(!Currency.class.isInstance(this.assets.get(i)))
			{
				continue;
			}
			if(((Currency) this.assets.get(i)).getSymbol().equals(symbol))
				return (Currency) this.assets.get(i);
		}
		return null;
	}
	
	public Broker getBrokerByName(String name)
	{
		for(int i=0; i<this.brokers.size(); i++)
		{
			if( this.brokers.get(i).getName().compareTo(name)==0)
				return this.brokers.get(i);
		}
		//this.artifact.print("C'est null ! dans une liste de " + this.brokers.size() + " de ne pas trouver " + name + " parmis " + this.brokers.get(0).getName()  +"\n");
		return null;
	}
	
	public AbstractTradingVenue getVenueByMic(String mic)
	{
		for(int i=0; i< venues.size(); i++)
			if(venues.get(i).getMic().compareTo(mic)==0)
				return venues.get(i);
		return null;
	}
	
	public AbstractTradingVenue getVenueByName(String name)
	{
		for(int i=0; i< venues.size(); i++)
			if(venues.get(i).getName().compareTo(name)==0)
				return venues.get(i);
		System.out.println("Error : venue '" + name + "' not found.");
		return null;
	}

	public ArrayList<Portfolio> getPortfolioOfAgent(String agentName)
	{
		ArrayList<Portfolio> agentPf = new ArrayList<Portfolio>();
		for(int i=0; i<portfolios.size(); i++)
		{
			if(portfolios.get(i).getHolder().compareTo(agentName)==0)
				agentPf.add(portfolios.get(i));
		}
		return agentPf;
	}
	
	public Portfolio getPortfolioByName(String portfolioName)
	{
		for(int i=0; i<portfolios.size(); i++)
		{
			if(portfolios.get(i).getName().compareTo(portfolioName)==0)
			return portfolios.get(i);
		}
		return null;
	}

	public MarketSimArtifact getArtifact()
	{
		return artifact;
	}

	public Date getDate() {
		return date;
	}
	
	public void setDate(Date date)
	{
		this.date= date;
	}
	
	@SuppressWarnings("deprecation")
	public void onClockTick(Date date)
	{
		setDate(date);
		for(int i=0; i<venues.size(); i++)
			venues.get(i).onTick(); // Propagate event to marketplaces
		
		if(previousDate==null && portfolios.size()>0)
		{
			System.out.println("First tick !");
			previousDate=date;
			for(int i=0; i<portfolios.size(); i++)
			{
				statsPortfolios.add(new StatisticsOutputByPortfolio(portfolios.get(i).getName(),this));
			}
			if(logImages)
				artifact.askForLogImages(); // update the logs on moral and ethical images of each agent toward the others
		}
		if(previousDate==null) return;
		
		if(date.getMinutes()!=previousDate.getMinutes())
		{
			// Firstly, we take the values of the assets
			ArrayList<Float> currentValuesOfAssets = new ArrayList<Float>();
			for(int a=0; a<assets.size(); a++)
			{
				currentValuesOfAssets.add((float) 0.001);
				AbstractAsset asset=assets.get(a);
				for(int i=0; i<venues.size(); i++)
				{
					if(venues.get(i).isQuoted(asset))
					{
						currentValuesOfAssets.set(a, venues.get(i).getMatchingMethod(asset.getName()).getLastClose());
					}
				}
			}
			// Then we print statistics for the portfolios
			for(int i=0; i<portfolios.size() && i<statsPortfolios.size(); i++)
			{
				statsPortfolios.get(i).putData(date);
				ArrayList<AbstractAsset> assetsToSet =portfolios.get(i).getAssets();
				for(int j=0; j< assetsToSet.size(); j++)
				{
					int n = portfolios.get(i).getVallueOfAssetByName(assetsToSet.get(j).getName());
					float p = currentValuesOfAssets.get(assets.indexOf(getAssetByName(assetsToSet.get(j).getName()))) * (float) n;
					statsPortfolios.get(i).putData(assetsToSet.get(j),n,p);
				}
				statsPortfolios.get(i).print();
			}
		}
		
		previousDate=date;
	}
	
	public void addAssetSubscriber(ArtifactId id, String venue, String asset)
	{
		AbstractTradingVenue v=getVenueByName(venue);
		AbstractMatchingMethod a = v.getMatchingMethod(asset);
		a.addSubscriber(id);
	}

	public ArrayList<AbstractAsset> getAssets() {
		return new ArrayList<AbstractAsset> (assets);
	}
	
	public ArrayList<AbstractTradingVenue> getVenues() {
		return new ArrayList<AbstractTradingVenue>(venues);
	}
	
	public boolean createPortfolio(String name, String holder, String broker)
	{	
		for(int i=0; i<portfolios.size(); i++) // We verify if another portfolio with the same name exists yet
			if(portfolios.get(i).getName().equals(name)) return false;
		
		Portfolio pf = new Portfolio(name, holder, broker);
		portfolios.add(pf);
		statsPortfolios.add(new StatisticsOutputByPortfolio(pf.getName(),this));
		return true;	
	}
	
	public void addInPortfolio(AbstractAsset asset, int qty, Portfolio pf)
	{
		pf.addAssets(asset, qty);
	}
}
