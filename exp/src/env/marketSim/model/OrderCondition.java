/**
 * 
 */
package marketSim.model;

/**
 * @author Nicolas
 *
 */
public enum OrderCondition
{
	
	IOC,
	/**
	 * Fill or Kill
	 */
	FOK,
	STOP,
	AON,
	MO,
	MF,
	/**
	 * No option
	 */
	NONE
}
