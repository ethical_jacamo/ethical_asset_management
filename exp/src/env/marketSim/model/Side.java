/**
 * 
 */
package marketSim.model;

/**
 * @author Nicolas
 *
 */
public enum Side {
	BID, ASK;
}
