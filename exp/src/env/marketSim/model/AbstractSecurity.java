package marketSim.model;

public abstract class AbstractSecurity extends AbstractAsset
{
	

	protected String isin;
	
	protected AbstractSecurity(String name, String symbol, String isin)
	{
		super(name, symbol);
		this.isin=isin;
	}
	
	protected AbstractSecurity(String name, String symbol, String isin, String comments)
	{
		super(name, symbol, comments);
		this.isin=isin;
	}
	
	public String getIsin() {
		return isin;
	}
}
